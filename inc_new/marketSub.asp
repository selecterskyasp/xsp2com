<%
'首页获取物流货运
function getWl(byVal ssum)
dim grs,tcontent,ti
set grs=server.CreateObject("adodb.recordset")
grs.open "select top "&ssum&" SMT_id,SMT_companyname,SMT_from,SMT_to,SMT_cid,smt_flx from wl_line where SMT_key=1 and SMT_flag=1 order by SMT_date,SMT_id desc",conn,1,1
if not(grs.bof and grs.eof) then
ti=1
tcontent=""
do while not grs.eof and ti<=ssum
tcontent=tcontent&""&vbcrlf
tcontent=tcontent&"<li>"&vbcrlf
tcontent=tcontent&"<h2>"&vbcrlf
tcontent=tcontent&"<a title=""从"&grs("SMT_from")&"到"&grs("SMT_to")&"的货运,由"&grs("SMT_companyname")&"公司发布"" href=""http://www.56zhw.com/line/detail.asp?id="&grs("SMT_id")&""" target=""_blank"">"&left(grs("SMT_from"),2)&"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&left(grs("SMT_to"),2)&"</a>"&vbcrlf
tcontent=tcontent&"</h2>"&vbcrlf
tcontent=tcontent&left(grs("SMT_companyname"),8)&vbcrlf
tcontent=tcontent&"<br />"&grs("smt_flx")&vbcrlf
tcontent=tcontent&"</li>"&vbcrlf
grs.movenext
ti=ti+1
loop
else
tcontent="暂无相关信息"
end if
grs.close
set grs=nothing
getWl=tcontent
end function

'获取产品分类 统计
function getFlList()
dim grs,grs1,gsql,tcontent,ti
dim tcount,tlen
ti=1
tcontent=""

gsql="select top 22 SMT_ypxxone,SMT_ypxxone_id from SMT_ypxxone order by SMT_date desc"
set grs=server.CreateObject("adodb.recordset")
set grs1=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=22
	tcount=conn.execute("select count(SMT_id) from SMT_cp where SMT_key=1 and SMT_key1=1 and SMT_key2=1 and SMT_newpro<>1 and SMT_ypxxone_id="&grs("SMT_ypxxone_id"))(0)	
	tcontent=tcontent&"<li><a href=""http://sell.xsp2.com/?bid="&grs("SMT_ypxxone_id")&""" target=""_blank""><strong>"&grs("SMT_ypxxone")&"</strong></a><span class=""gray"">("&tcount&")</span> <br />"&vbcrlf
	grs1.open "select top 6 SMT_ypxxtwo_id,SMT_ypxxtwo from SMT_ypxxtwo where SMT_ypxxone_id="&grs("SMT_ypxxone_id")&" order by SMT_date desc",conn,1,1
	if not grs1.eof then
		tlen=0
		do while not grs1.eof
			tlen=tlen+len(grs1("SMT_ypxxtwo"))	
			if tlen>15 then exit do	
  			tcontent=tcontent&"<a href=""http://sell.xsp2.com/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a>&nbsp;<span class=""gray"">|</span>&nbsp;"
			
			grs1.movenext
		loop
		'去掉右边的 |
		tcontent=left(tcontent,len(tcontent)-39)
	else
		tcontent=tcontent&"<a href=""javascript:;"">暂无小类</a>"
	end if
	grs1.close	
	tcontent=tcontent&"</li>"&vbcrlf
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
set grs1=nothing
getFlList=tcontent
end function

'获取批发市场
'snum 条件 sWhere 附加条件  只在分页显示中有效
'smode 0为首页推荐 1为详细页推荐 2为市场列表
function getMarketList(byVal snum,byVal sWhere,byVal smode)
dim grs,gsql,tcontent,ti
dim tpage
dim turl,tt,tpic
ti=1
tcontent=""
select case smode
case 0
gsql="select SMT_id,SMT_pic,SMT_title from SMT_market where SMT_flag=1 order by SMT_flagtime desc"
case 1
gsql="select SMT_id,SMT_title,SMT_pic from SMT_market where SMT_flag=1 order by SMT_flagtime desc"
case 2
	tpage=request.QueryString("page")
	tcontent="<ul>"
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=1
	if tpage=1 then
		gsql="select top "&snum&" SMT_id,SMT_pic,SMT_title from SMT_market where "&sWhere&" order by SMT_id desc"
	else
		gsql="SELECT TOP "&snum&" SMT_id,SMT_pic,SMT_title from SMT_market  where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((tpage-1)*snum)&" SMT_id FROM SMT_market where "&sWhere&" order by SMT_id desc) AS tblTMP)) and "&sWhere&" order by SMT_id desc"  			
	end if
end select
set grs=server.CreateObject("adodb.recordset")

grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum
		turl="market.asp?id="&grs("SMT_id")
		tt=grs("SMT_title")
		tpic=grs("SMT_pic")		
		if chkNull(tpic,20) then tpic="/images/noimg.gif"
		select case smode
		case 0
			tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" border=""0"" class=""img"" width=""80"" height=""80"" /></a>"&vbcrlf
			tcontent=tcontent&"<span><a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,8)&"</a></span></li>"&vbcrlf
		case 1
			tcontent=tcontent&"<li>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,10)&"</a></li>"&vbcrlf
		case 2
			keyword=checkstr(request.QueryString("keyword"))
			tt2=left(tt,9)
			if chkRange(keyword,1,25) then
				if len(keyword)>3 then	keyword=getkeyword(keyword)(1)
				tt2=boldkeyword(tt2,keyword,0)
			end if
			tcontent=tcontent&"<li>"&vbcrlf
			tcontent=tcontent&"<ul class=""pro"">"&vbcrlf
			tcontent=tcontent&"<li onmouseover=""this.className='hover'"" onmouseout=""this.className=''"">"&vbcrlf
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" border=""0"" width=""100"" height=""100"" /></a>"&vbcrlf
			tcontent=tcontent&"<span><a href="""&turl&""">"&tt2&"</a></span>"&vbcrlf
			tcontent=tcontent&"</li>"&vbcrlf
			tcontent=tcontent&"</ul>"&vbcrlf
			tcontent=tcontent&"</li>"&vbcrlf
			if ti mod 6=0 then
			tcontent=tcontent&"</ul><ul>"&vbcrlf
			end if
		end select		
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
if smode=2 then
tcontent=tcontent&"</ul>"&vbcrlf
end if
getMarketList=tcontent
end function

'获取批发市场商铺函数
'snum 条件 sWhere 附加条件  只在分页显示,详细页推荐中有效
'smode 0为首页推荐 1为详细页推荐 2为市场列表
function getMarketShopList(byVal snum,byVal sWhere,byVal smode)
dim grs,gsql,tcontent,ti
dim tpage
dim turl,tt,tpic,tdate,tjy
dim tst
ti=1
tcontent=""
select case smode
case 0
gsql="select top "&snum&" SMT_user,SMT_coname,SMT_logo from SMT_yp where SMT_key=3 and SMT_key1=1 and SMT_marketid>1 and SMT_marketcid>1 and SMT_cojy='市场经销商' "&sWhere&" order by SMT_date desc"
case 1
gsql="select top "&snum&" SMT_user,SMT_coname,SMT_logo,SMT_coyw,SMT_colxr,SMT_cotelq,SMT_cotel,SMT_mail from SMT_yp where SMT_key=3 and SMT_key1=1 "&sWhere&" and SMT_cojy='市场经销商' order by SMT_date desc"
case 2
	tpage=request.QueryString("page")
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=1
	if tpage=1 then
		gsql="select top "&snum&" SMT_id,SMT_logo,SMT_user,SMT_coname,SMT_coaddress,SMT_coyw,SMT_date,SMT_qqnum from SMT_yp where (SMT_key=1 or SMT_key=3) and SMT_key1=1 and SMT_marketid>1 and SMT_marketcid>1 and SMT_cojy='市场经销商' "&sWhere&" order by SMT_id desc"
	else
		gsql="SELECT TOP "&snum&" SMT_id,SMT_logo,SMT_user,SMT_coname,SMT_coaddress,SMT_coyw,SMT_date,SMT_qqnum from SMT_yp  where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((tpage-1)*snum)&" SMT_id FROM SMT_yp where (SMT_key=1 or SMT_key=3) and SMT_key1=1 and SMT_marketid>1 and SMT_marketcid>1 and SMT_cojy='市场经销商' "&sWhere&" order by SMT_id desc) AS tblTMP)) and (SMT_key=1 or SMT_key=3) and SMT_key1=1 and SMT_marketid>1 and SMT_marketcid>1 and SMT_cojy='市场经销商' "&sWhere&" order by SMT_id desc"  			
	end if
end select
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum
		turl="http://"&grs("SMT_user")&".xsp2.com"
		tt=grs("SMT_coname")
		tpic=grs("SMT_logo")		
		if chkNull(tpic,20) then tpic="/images/noimg.gif"
		select case smode
		case 0
			tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" border=""0"" class=""img"" width=""80"" height=""80"" /></a>"&vbcrlf
			tcontent=tcontent&"<span><a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,8)&"</a></span></li>"&vbcrlf
		case 1		
			tcontent=tcontent&"<li>"&vbcrlf
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" border=""0"" class=""img"" width=""100"" height=""100"" /></a> "&vbcrlf
			tcontent=tcontent&"<h2><a href="""&turl&""" target=""_blank"" title="""&tt&""" class=""blue"">"&left(tt,9)&"</a></h2>"&vbcrlf
			tcontent=tcontent&"主营："&left(grs("SMT_coyw"),7)&"<br />"&vbcrlf
			tcontent=tcontent&"联系人："&left(grs("SMT_colxr"),4)&"<br />"&vbcrlf
			tcontent=tcontent&"电话："&left(grs("SMT_cotelq"),4)&"-"&left(grs("SMT_cotel"),8)&"<br />"&vbcrlf
			tcontent=tcontent&"邮箱："&grs("SMT_mail")&vbcrlf
			tcontent=tcontent&"</li>"&vbcrlf
		case 2	
			if ti mod 2=0 then tst="2" else tst=""
			tcontent=tcontent&"<ul>"&vbcrlf
			tcontent=tcontent&"<li class=""shopcontent1"&tst&"""><a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" alt="""&tt&""" border=""0"" class=""img"" width=""100"" height=""100"" /></a></li>"&vbcrlf
			tcontent=tcontent&"<li class=""shopcontent2"&tst&""">"&vbcrlf
			tcontent=tcontent&"<div class=""product"">"&vbcrlf
			tcontent=tcontent&"<h1><a href="""&turl&""" target=""_blank"" title="""&tt&""" class=""link"">"&tt&"</a></h1>"&vbcrlf
			tcontent=tcontent&"</div>"&vbcrlf
			tcontent=tcontent&"<div class=""time"">"&formatdatetime(grs("SMT_date"),2)&"</div>"&vbcrlf
			tcontent=tcontent&"<div class=""description"">经营范围："&grs("SMT_coyw")&"</div>"&vbcrlf
			tcontent=tcontent&"<div class=""description"">地址："&grs("SMT_coaddress")&"</div>"&vbcrlf
			tcontent=tcontent&"</li>"&vbcrlf		
			tcontent=tcontent&"<li class=""shopcontent3"&tst&"""><a href="""&turl&"/guestbook.asp"" target=""_blank"" title="""&tt&"""><img src=""/images/qq.gif"" width=""77"" height=""17"" border=""0"" /></a></li>"&vbcrlf		
			tcontent=tcontent&"</ul>"&vbcrlf					
		end select		
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
getMarketShopList=tcontent
end function

'获取热门产品 推荐产品
function getHotProduct()
dim grs,gsql,tcontent,ti
dim turl,tt,tpic
ti=1
tcontent=""
gsql="select top 8 SMT_id,SMT_yp_id,SMT_pic,SMT_cpname from SMT_cp where SMT_key=1 and SMT_key1=1 and SMT_key2=1 and SMT_key3=1 and SMT_newpro<>1 order by SMT_date desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=8	
	turl="http://www.xsp2.com/sell/detail/"&grs("SMT_yp_id")&"_"&grs("SMT_id")&".html"
	tt=grs("SMT_cpname")
	tpic=grs("SMT_pic")
	if chkNull(tpic,20) then tpic="/images/noimg.gif"
	tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" titile="""&tt&"""><img src="""&tpic&""" border=""0"" class=""img"" alt="""&tt&""" width=""80"" height=""80""/></a> <br />"&vbcrlf
	tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" titile="""&tt&""">"&left(tt,8)&"</a></li>"&vbcrlf
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
getHotProduct=tcontent
end function
%>
