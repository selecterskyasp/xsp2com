<%
function getKeyword(byval fk)
fk=trim(fk)
fk=replace(fk,"|","")
if len(fk)<4 or len(fk)>25 then
	getKeyword=array(0,fk)
	exit function
end if

dim obj
Set obj = Server.CreateObject("ACWPSCOM.ACWPS")

if obj.init_Dict()<>1 then
	getKeyword=array(1,"加载字典失败")
	exit function
end if

getKeyword=array(0,obj.doCWP(fk,12, 124))
obj.release_Dict()
set obj=nothing
end function

'生成sql语句 f_数据字段，多个用,分隔 str_关键字，多个用|分隔 smode连接模式 0用and连接 1用or连接
function createSql(f_,str_,smode)
if len(trim(f_))<1 or len(trim(str_))<1 then
	createSql=array(1,"参数不正确")
	exit function
end if
dim i,j,tmparr,tmparr2,st,op
tmparr=split(f_,",")
if right(str_,1)="|" then str_=left(str_,len(str_)-1)
tmparr2=split(str_,"|")
if smode=0 then op=" and " else op=" or "
st=""
for i=0 to ubound(tmparr)
	if len(trim(tmparr(i)))>0 then
		for j=0 to ubound(tmparr2)
			if len(trim(tmparr2(j)))>0 then
				if st="" then 
					st="(charindex('"&trim(tmparr2(j))&"',"&tmparr(i)&")>0" 
				else
					if right(st,1)<>"(" then
					st=st&op&"charindex('"&trim(tmparr2(j))&"',"&tmparr(i)&")>0"
					else
					st=st&"charindex('"&trim(tmparr2(j))&"',"&tmparr(i)&")>0"
					end if
				end if
			end if
		next
		if i<ubound(tmparr) then st=st&") or (" else st=st&")"		
	end if
next
createSql=array(0,st)
erase tmparr:erase tmparr2:st=""
end function

'加亮关键字 str1:要加亮的字符串 str2：加亮的关键字 多个用|分隔 smode:模式 0加亮且加粗 1只加亮 2只加粗
function boldKeyword(byval str1,byval str2,smode)
str1=trim(str1):str2=trim(str2)
if len(str1)<2 or len(str2)<1 or len(str1)<len(str2) then
	boldKeyword=str1
	exit function
end if
dim h,e
h="<span style=""color:#ff0000;font-weight:800"">"
e="</span>"
select case smode
case 0
	h="<span style=""color:#ff0000;font-weight:800"">"
case 1
	h="<span style=""color:#ff0000"">"
case 2
	h="<span style=""font-weight:800"">"
end select
if right(str2,1)="|" then str2=left(str2,len(str2)-1)
dim tmparr,i,tmp
tmparr=split(str2,"|")
for i=0 to ubound(tmparr)
	tmp=trim(tmparr(i))
	if len(tmp)>1 then
		str1=replace(str1,tmp,h&tmp&e)
	end if
next
boldKeyword=str1
erase tmparr:str1="":str2=""
end function

function keywordtest()
dim st,f,k
st="义乌亲亲小饰品厂集生产和销售为一体，产品远销韩国、日本、欧美等几十个国家和地区，经营范围包括韩国饰品"
f="义乌"
k=getkeyword(st)(1)
response.Write(k&"<br>")
response.Write(createsql("name,title",k,0)(1)&"<br>")
response.Write(boldKeyword(st,k,0)&"<br>")
end function
%>