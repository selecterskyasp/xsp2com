﻿<%
'会员发布的各种信息过滤
Function Replace_Text(byVal fString)
If Not IsNull(fString) Then
'fString = trim(fString)
fString = Replace(fString, CHR(10), "<BR> ")
fString = replace(fString, ">", "&gt;")
fString = replace(fString, chr(62), "&gt;") '>
fString = replace(fString, "<", "&lt;")
fString = replace(fString, chr(60), "&lt;") '<

fString = Replace(fString, CHR(34), "&quot;")
fString = Replace(fString, CHR(39), "&quot;")	'单引号过滤
fString = replace(fString, "'", "&quot;")
fString = replace(fString, """", "&quot;")
fString = Replace(fString, CHR(32), " ")		'&nbsp;
fString = Replace(fString, CHR(9), " ")			'&nbsp;
fString = Replace(fString, CHR(13), "")
Replace_Text = fString
End If
End Function
'转换
Function replace_T(byVal fstring)
If Not IsNull(fString) Then
'fString = trim(fString)
fString = Replace(fString, "&lt;", "<",1,-1,1)
fString = Replace(fString, "&gt;", ">",1,-1,1)
fString = Replace(fString, "<"&chr(37), "&lt;%",1,-1,1) '过滤< % % >
fString = Replace(fString, chr(37)&">", "%&gt;",1,-1,1)
fString = Replace(fString, "<script", "&lt;script",1,-1,1) '不完全过滤<script>< /script>
fString = Replace(fString, "</script>", "&lt;/script&gt;",1,-1,1)
replace_T = fString
end if 
end Function

'完全过滤html
Function getInnerText(byVal strHTML)
strHTML=trim(strHTML)
  if strHTML="" or isnull(strHTML) then
    getInnerText="":exit function
  end if
    Dim objRegExp, Match, Matches 
    Set objRegExp = New Regexp
    
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    '取闭合的<>
    objRegExp.Pattern = "<.+?>"
    '进行匹配
    Set Matches = objRegExp.Execute(strHTML)
    
    ' 遍历匹配集合，并替换掉匹配的项目
    For Each Match in Matches 
    strHtml=Replace(strHTML,Match.Value,"")
    Next
	strHTML=replace(strHTML,"&nbsp;","")
	strHTML=replace(strHTML," ","")	
    getInnerText=strHTML
    Set objRegExp = Nothing
End Function

'取完全过滤的html的字符串数
Function LeftT(byVal str,byVal n)
  if str="" or isnull(str) then
    LeftT="":exit function
  end if
str=getInnerText(str)
str=left(str,n)
LeftT=str
'
'If len(str)<=n/2 Then
'LeftT=str
'Else
'Dim TStr
'Dim l,t,c
'Dim i
'
'l=len(str)
'if l > 0 then 
'	TStr=""
'	t=0
'	for i=1 to l
'	c=asc(mid(str,i,1))
'	If c<0 then c=c+65536
'	If c>255 then
'	t=t+2
'	Else
'	t=t+1
'	End If
'	If t>n Then exit for
'	TStr=TStr&(mid(str,i,1))
'	next
'	LeftT = TStr
'	End If
'end if
End Function

'过滤SQL非法字符
Function checkStr(byVal Chkstr)
	dim Str:Str=trim(Chkstr)
	if isnull(Str) then
		checkStr = ""
		exit Function
	else
		Str=trim(Str)
		Str=replace(Str,"'","")	
		checkStr=getInnerText(Str)
	end if
End Function

'检测传递的参数是否为数字型
Function Chkrequest(byVal Para)
Chkrequest=False
Para=trim(Para)
If not IsNull(Para) and not Para="" and IsNumeric(Para) Then
	tempNum=clng(Para)
	if tempNum>0 then
  	 Chkrequest=True
   	end if
End If
End Function

'检查字符串是满足长度
Function ChkNull(byVal Para,byVal strlen)
dim tempPara:tempPara=trim(Para)
if isnull(tempPara)=true or tempPara="" or len(tempPara)<strlen then
ChkNull=True
else
ChkNull=False
end if
end Function

'检查字符串满足长度的范围 如果满足，返回True 否则，返回False
function chkRange(byVal Para,byVal smin,byVal sMax)
dim tempPara:tempPara=trim(Para)
if isnull(tempPara) or tempPara="" or len(tempPara)<smin or len(tempPara)>sMax then
chkRange=False
else
chkRange=True
end if
end function

'返回定向文件
function getDirect()
dim tempUrl
tempUrl=checkstr(request.Cookies("direct"))
if not chkRange(tempUrl,14,100) then
getDirect=null
exit function
end if
tempUrl=lcase(tempUrl)
if instr(tempUrl,"http://")<1 or instr(tempUrl,"xsp2.com")<1 then
getDirect=null
exit function
end if
getDirect=tempUrl
end function

'********************************************
'函数名：IsValidEmail
'作  用：检查Email地址合法性
'参  数：email ----要检查的Email地址
'返回值：True  ----Email地址合法
'       False ----Email地址不合法
'********************************************
function IsValidEmail(byVal email)
	dim names, name, i, c
	IsValidEmail = true
	names = Split(email, "@")
	if UBound(names) <> 1 then
	   IsValidEmail = false
	   exit function
	end if
	for each name in names
		if Len(name) <= 0 then
			IsValidEmail = false
    		exit function
		end if
		for i = 1 to Len(name)
		    c = Lcase(Mid(name, i, 1))
			if InStr("abcdefghijklmnopqrstuvwxyz_-.", c) <= 0 and not IsNumeric(c) then
		       IsValidEmail = false
		       exit function
		     end if
	   next
	   if Left(name, 1) = "." or Right(name, 1) = "." then
    	  IsValidEmail = false
	      exit function
	   end if
	next
	if InStr(names(1), ".") <= 0 then
		IsValidEmail = false
	   exit function
	end if
	i = Len(names(1)) - InStrRev(names(1), ".")
	if i <> 2 and i <> 3 then
	   IsValidEmail = false
	   exit function
	end if
	if InStr(email, "..") > 0 then
	   IsValidEmail = false
	end if
end function

'检查是否汉字 true是，false否
function chkChinese(byVal para)
	if chkNull(para,1) then 
	chkChinese=false
	exit function
	end if
	Set RegExpObj=new RegExp   
	RegExpObj.Pattern="^[\u4e00-\u9fa5]+$"   
	ReGCheck=RegExpObj.test(para) 
	Set RegExpObj=nothing
	chkChinese=ReGCheck
end function

'检查用户名是否合法 字母开头，4-20位的数字，字母，下划线组合
Function isValidName(byVal str)
    Dim re
    Set re = New RegExp
    re.IgnoreCase = True
    re.Global = True
    re.Pattern = "^[A-Za-z][A-Za-z0-9_]{3,19}$"
    isValidName = re.Test(str)
    Set re = Nothing
End Function 

'检测传递的参数是否为日期型
Function Chkrequestdate(byVal Para)
Chkrequestdate=False
If Not (IsNull(Para) Or Trim(Para)="" Or Not IsDate(Para)) Then
   Chkrequestdate=True
End If
End Function

sub insert_record(byVal table,byVal Parameters,byVal values)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	conn.execute(sql)
end sub

sub del_record(byVal table,byVal Conditions)'表名,条件,返回路径
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	conn.execute("delete from "&table&" where "&Conditions&"")
end sub

sub update_record(byVal table,byVal Parameters,byVal Conditions)'表名,条件,返回路径
	'response.Write("update "&table&" set "&Parameters&" where "&Conditions&"")
'response.End()
	conn.execute("update "&table&" set "&Parameters&" where "&Conditions&"")
end sub
Function select_recordset(byVal fieldname,byVal tablename,byVal v_fieldname,byVal v_fieldname_value)

	  sql_view="select "&fieldname&" from "&tablename&" where  "&v_fieldname&"="&v_fieldname_value&""
	  set rs_view=server.CreateObject("adodb.recordset")
	  rs_view.open sql_view,conn
	     if not rs_view.eof then
	        select_recordset=rs_view(fieldname)
	     end if 
	  rs_view.close
	  set rs_view=nothing
 end Function
'关闭对象
sub closers(Para)	
	if typename(Para)<>"Nothing" and isobject(Para) then
		if Para.state<>0 then Para.close	
		set Para=nothing
	end if
end sub

'释放资源
sub closeconngoto()'
	if typename(conn)<>"Nothing" and isobject(Para) then
		if conn.state<>0 then conn.close	
		set conn=nothing	
	end if
end sub
sub closeconn()'会跳转
	call closeconngoto()
	response.Cookies("direct").domain="xsp2.com"	
	response.Cookies("direct").path="/"
	response.Cookies("direct")="http://"&Request.ServerVariables("HTTP_HOST")&Request.ServerVariables("URL")&"?"&Request.ServerVariables("QUERY_STRING")	
end sub

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:"&msg&"');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function

'成功信息提示
function showmsg(byVal ref)
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('操作成功!');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function
'检查是否登录(不完全检查) true已经登录 false没有登录
function chkLogin()
	if chkNull(request.Cookies("user")("name"),4) then
	chkLogin=false
	else
	chkLogin=true
	end if
end function

'检查是否登录(完全检查)
function chkTrueLogin()
dim username,userid,pass
if not request.Cookies("user").HasKeys then
chkTrueLogin=false
exit function
end if

username=checkstr(request.Cookies("user")("name"))
userid=checkstr(request.Cookies("user")("id"))
pass=checkstr(request.Cookies("user")("pass"))

if chkNull(username,4) or not chkrequest(userid) or chkNull(pass,32) then
chkTrueLogin=false
exit function
end if

set isLoginRS=conn.execute("select top 1 SMT_id from SMT_yp where SMT_user='"&username&"' and SMT_id="&clng(userid)&" and SMT_pass='"&pass&"'")

if isLoginRs.bof and isLoginRs.eof then 
isLoginRs.close
set isLoginRs=nothing
chkTrueLogin=false
exit function
end if
chkTrueLogin=true
end function

'显示欢迎用户登录的信息
function showWelcome()
showWelcome="您好！<span class=""red"">"&request.Cookies("user")("name")&"</span>，欢迎回来， <a href='/member/' class=""red"">会员中心</a> <a href=""/member/logout.html"" class=""red"">退出</a>"
end function

'显示需要登录的信息
function showLogin()
showLogin="<a href=""/reg/login.html""><img src=""/images/hydl.gif"" border=""0"" /></a> <a href=""/reg/join.html""><img src=""/images/mfzc.gif"" border=""0"" /></a> <a href=""/issue/""><img src=""/images/ksfb.gif"" border=""0"" /></a>"
end function

'================================================= 
  '函数名：JmailSend 
  '作 用：用Jmail发送邮件 
  '参 数：MailTo收件人地址 Subject邮件标题 Body内容 
  '返回值：flase 发送失败　true 发送成
function JmailSend(byVal MailTo,byVal Subject,byVal Body) 
   dim JmailMsg,From,FromName,Smtp,Username,Password
  ' Body 邮件内容    
  ' MailTo 收件人Email 
  ' From 发件人Email 
  ' FromName 发件人姓名 
  ' Smtp smtp服务器 
  ' Username 邮箱用户名 
  ' Password 邮箱密码 
   From="root@xsp2.com"
   FromName="小商品资源网"
   Smtp="mail@xsp2.com"
   Username="root@xsp2.com"
   Password="wujinchen"
   
   set JmailMsg=server.createobject("jmail.message") 
   JmailMsg.charset="uft-8" 
   JmailMsg.ContentType = "text/html" 
   JmailMsg.ISOEncodeHeaders = False '是否进行ISO编码，默认为True
   JmailMsg.AddHeader "Originating-IP", Request.ServerVariables("REMOTE_ADDR")
   JmailMsg.from=From
   JmailMsg.AddRecipient MailTo 
   JmailMsg.MailDomain=Smtp      
   JmailMsg.MailServerUserName =Username
   JmailMsg.MailServerPassWord =Password     
   JmailMsg.fromname=FromName     
   JmailMsg.subject=Subject 
   JmailMsg.body=Body       
   JmailSend=JmailMsg.send("mail.xsp2.com")    
   JmailMsg.close 
   set JmailMsg=nothing    
end function 

'********************************************
'函数名：makePassword
'作  用：产生指定长度的随机数字
'参  数：maxLen ----随机数的长度
'返回值：产生的随机数
'********************************************
function   makePassword(byVal   maxLen)   
  Dim   strNewPass   
  Dim   whatsNext,   upper,   lower,   intCounter   
  Randomize      
  For   intCounter   =   1   To   maxLen   
  whatsNext   =   Int((1   -   0   +   1)   *   Rnd   +   0) 
  upper   =   57   
  lower   =   48   
  strNewPass   =   strNewPass   &   Chr(Int((upper   -   lower   +   1)   *   Rnd   +   lower))   
  Next   
  makePassword   =   strNewPass       
end   function

'查找网址是否为有效的网址，用于自动定向的cookies检测
function chkUrl(byVal url)
chkUrl=false
url=lcase(url)
if (instr(url,"xsp2.com")<1 and instr(url,"xsp2.net")<1 and instr(url,"xsp2.cn")<1) or len(url)>100 then
chkUrl=false
else
chkUrl=true
end if
end function

'得到价格 如果价格为0，刚返回面议 para价格 sdot小数位数
function getPrice(byVal para,byVal sdot)
if isnull(para) or para="" or len(para)<1 then
	getPrice="面议"
	exit function
end if
para=cdbl(para)
if para=0 then
	getPrice="面议"
	exit function
end if
para=formatnumber(para,sdot)
if left(cstr(para),1)="." then para="0"+cstr(para) else para=cstr(para)
getPrice=para
end function

'删除地址中的参数
'surl 地址
'spara 要查找的参数
function delUrlpara(byVal surl,byVal spara)
dim pos1,pos2
dim tmpurl
spara=lcase(spara)
surl=lcase(surl)
spara=spara&"="
pos1=instr(surl,spara)
if pos1>0 then
	if pos1=1 then
		'在第一个位置,前面没有参数		
		pos2=instr(pos1,surl,"&")
		if pos2<>0 then
		tmpurl=right(surl,len(surl)-pos2+1)		
		else
		tmpurl=""
		end if
	else
		'不在第一个位置,前面以&开头的			
		if mid(surl,pos1-1,1)="&" then 
			tmpurl=left(surl,pos1-2)
			pos2=instr(pos1,surl,"&")
			if pos2>pos1 then tmpurl=tmpurl&right(surl,len(surl)-pos2+1) 
		elseif mid(surl,pos1-1,1)="?" then
			'是带有网址的参数
			tmpurl=left(surl,pos1-1)
			pos2=instr(pos1,surl,"&")
			if pos2>pos1 then tmpurl=tmpurl&right(surl,len(surl)-pos2+1) 
		else
			'没有找到
			tmpurl=surl
		end if
	end if
else
	'没有找到
	tmpurl=surl
end if
tmpurl=replace(replace(tmpurl,"?&","?"),"&&","&")
if right(tmpurl,1)="&" or right(tmpurl,1)="?" then tmpurl=left(tmpurl,len(tmpurl)-1)
if left(tmpurl,1)="&" then tmpurl=right(tmpurl,len(tmpurl)-1)
delUrlpara=tmpurl
end function
%>
