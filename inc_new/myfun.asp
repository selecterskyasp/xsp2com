<%
function chkPara(byVal para)
	if para="" or isnull(para) or isempty(para) then
		chkPara=false
		exit function
	end if
	dim regEx
	set regEx=new regexp
	regEx.global=true
	regEx.pattern="^\d+(,?\d+)*$"
	chkPara=regEx.test(para)
	set regEx=nothing
end function

'获取地址 根据ID（以逗号分隔）获取class表里面的name，以空格分隔
function getNameList(svalue)
dim tmparr,i
getNameList=""
if not chkpara(svalue) then exit function
sql="select id,name from class where id in("&svalue&") order by charindex(ltrim(id),'"&svalue&"')"
tmparr=getDBvalueList(sql)
if clng(tmparr(0,0))<>0 then
	for i=0 to ubound(tmparr,2)
		if getNameList="" then getNameList=tmparr(1,i) else getNameList=getNameList&"&nbsp;"&tmparr(1,i)
	next
end if
erase tmparr
end function

function getDingdanState(flagid)
flagid=cint(flagid)
getDingdanState="新订单，等待付款"
select case flagid
case 1
getDingdanState="新订单，等待买家付款"
case 2
getDingdanState="已经付款，等待卖家确认"
case 3
getDingdanState="货已发"
end select
end function

'写cookies 
'scookiesarr： 组名 如果为空则不使用 scookiesname：cookies名 svalue:值
function writecookies(scookiesarr,scookiesname,svalue)
if scookiesarr="" then
	response.Cookies(scookiesname).domain="xsp2.com" 
	response.Cookies(scookiesname).path="/"
	Response.Cookies(scookiesname).Expires=dateadd("h",12,now())
	response.Cookies(scookiesname)=svalue
else
	response.Cookies(scookiesarr).domain="xsp2.com"
	response.Cookies(scookiesarr).path="/"
	Response.Cookies(scookiesarr).Expires=dateadd("h",12,now())
	response.Cookies(scookiesarr)(scookiesname)=svalue
end if
end function

'都cookies
'scookiesarr： 组名 如果为空则不使用 scookiesname：cookies名
'返回值 如果没有则返回 ''
function readcookies(scookiesarr,scookiesname)
dim t
if scookiesarr="" then	
	t=request.Cookies(scookiesname)
else	
	t=request.Cookies(scookiesarr)(scookiesname)
end if
if chknull(t,1) then t="" else t=checkstr(t)
readcookies=t
end function
sub insert_record(table,Parameters,values)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	call setdbvalue(sql)
end sub

sub del_record(table,Conditions)'表名,条件,返回路径
sql="delete from "&table&" where "&Conditions&""
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	call setdbvalue(sql)
end sub

sub update_record(table,Parameters,Conditions)'表名,条件,返回路径
	'response.Write("update "&table&" set "&Parameters&" where "&Conditions&"")
'response.End()
sql="update "&table&" set "&Parameters&" where "&Conditions&""
	call setdbvalue(sql)
end sub
Function select_recordset(fieldname,tablename,v_fieldname,v_fieldname_value)

	  sql_view="select "&fieldname&" from "&tablename&" where  "&v_fieldname&"="&v_fieldname_value&""
	  set rs_view=server.CreateObject("adodb.recordset")
	  rs_view.open sql_view,conn
	     if not rs_view.eof then
	        select_recordset=rs_view(fieldname)
	     end if 
	  rs_view.close
	  set rs_view=nothing
 end Function
'关闭对象
sub closers(Para)	
	if typename(Para)<>"Nothing" and isobject(Para) then
		if Para.state<>0 then Para.close	
		set Para=nothing
	end if
end sub

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:"&msg&"');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function

'成功信息提示
function showmsg(byVal ref)
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('操作成功!');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function
'检查是否登录(不完全检查) true已经登录 false没有登录
function chkLogin()
	if chkNull(request.Cookies("user")("name"),4) then
	chkLogin=false
	else
	chkLogin=true
	end if
end function

'检查是否登录(完全检查)
function chkTrueLogin()
dim username,userid,pass
if not request.Cookies("user").HasKeys then
chkTrueLogin=false
exit function
end if

username=checkstr(request.Cookies("user")("name"))
userid=checkstr(request.Cookies("user")("id"))
pass=checkstr(request.Cookies("user")("pass"))

if chkNull(username,4) or not chkrequest(userid) or chkNull(pass,32) then
chkTrueLogin=false
exit function
end if
sql="select top 1 SMT_id from SMT_yp where SMT_user='"&username&"' and SMT_id="&clng(userid)&" and SMT_pass='"&pass&"'"
tmparr=getdbvalue(sql,1)
if tmparr(0)=0 then
chkTrueLogin=false
else
chkTrueLogin=true
end if
erase tmparr
end function

function MemberLogout()
response.Cookies("user").domain="xsp2.com"
response.Cookies("user").path="/"
response.Cookies("user")("name")=""
response.Cookies("user")("id")=""
response.Cookies("user")("pass")=""
response.Cookies("user")("st")=""
session("userid")=""
end function

'********************************************
'函数名：makePassword
'作  用：产生指定长度的随机数字
'参  数：maxLen ----随机数的长度
'返回值：产生的随机数
'********************************************
function   makePassword(byVal   maxLen)   
  Dim   strNewPass   
  Dim   whatsNext,   upper,   lower,   intCounter   
  Randomize      
  For   intCounter   =   1   To   maxLen   
  whatsNext   =   Int((1   -   0   +   1)   *   Rnd   +   0) 
  upper   =   57   
  lower   =   48   
  strNewPass   =   strNewPass   &   Chr(Int((upper   -   lower   +   1)   *   Rnd   +   lower))   
  Next   
  makePassword   =   strNewPass       
end   function

'产生18位订单号
function MakeDingdanNumber()
dim ttime
dim tyear,tmonth,tday,thour,tminute,tsecond,trndNum
const tRndMax=4
ttime		= now()
tyear		= year(ttime)
tmonth		= month(ttime)
tday		= day(ttime)
thour		= hour(ttime)
tminute		= minute(ttime)
tsecond		= second(ttime)
if tmonth<10 then tmonth="0"&tmonth
if tday<10 then	tday="0"&tday
if thour<10 then thour="0"&thour
if tminute<10 then tminute="0"&tminute
if tsecond<10 then tsecond="0"&tsecond
'时间产生后共14位

'获取4位随机数字
trndNum=getRndNum(tRndMax)
MakeDingdanNumber=tyear&tmonth&tday&thour&tminute&tsecond&trndNum
end function

'查找网址是否为有效的网址，用于自动定向的cookies检测
function chkUrl(byVal url)
chkUrl=false
url=lcase(url)
if (instr(url,"xsp2.com")<1 and instr(url,"xsp2.net")<1 and instr(url,"xsp2.cn")<1) or len(url)>100 then
chkUrl=false
else
chkUrl=true
end if
end function

''得到价格 如果价格为0，刚返回面议 para价格 sdot小数位数
'function getPrice(byVal para,byVal sdot)
'if isnull(para) or para="" or len(para)<1 then
'	getPrice="面议"
'	exit function
'end if
'para=cdbl(para)
'if para=0 then
'	getPrice="面议"
'	exit function
'end if
'para=formatnumber(para,sdot)
'if left(cstr(para),1)="." then para="0"+cstr(para) else para=cstr(para)
'getPrice=para
'end function

'得到类别名称
function getClassName(feild,table,wheres)'feild:字段,table:表名,wheres:条件
	sql="select top 1 "&feild&" from "&table&" where "&wheres
	tmparr=getdbvalue(sql,1)
	if tmparr(0)<>0 then	
		getClassName = tmparr(1)
	else
		getClassName = ""
	end if
	erase tmparr
end function

'释放资源
sub closeconngoto()'
	if typename(conn)<>"Nothing" and isobject(Para) then
		if conn.state<>0 then conn.close	
		set conn=nothing	
	end if
end sub

'设置购物车产品数量、产品总金额cookies
function setCartCookies(spnum,spmoney)
call writecookies("","buyproductnum",spnum)
call writecookies("","buyproductmoney",clng(spmoney))
end function

'检查购物车产品是否正确
function chkShopcart()
dim st,tst
dim tmparr,tmparr1,ti,typto_id,istrue
st=readcookies("","shopcart")
if chkNull(st,5) or instr(st,":")=0 then 
chkShopcart=array(1,"长度不对")
exit function
end if
tmparr=split(st,"|")
typto_id=0
istrue=false
for ti=0 to ubound(tmparr)
	tst=tmparr(ti)
	tmparr1=split(tst,":")
	if ubound(tmparr1)<>2 then 
		chkShopcart=array(2,"产品参数不对")
		exit function
	end if	
	if chkNumber(tmparr1(0)) and chkRequest(tmparr1(1)) and chkrequest(tmparr1(2)) then
		if typto_id=0 then 
		typto_id=clng(tmparr1(2))
		istrue=true
		else
			if typto_id<>clng(tmparr1(2)) then				
				chkShopcart=array(3,"一次只能在同一个企业下单")
				exit function
			else
				istrue=true
			end if
		end if
	else
		chkShopcart=array(4,"产品ID，产品数量，企业ID非数字")		
		exit function
	end if	
next
if istrue then chkShopcart=array(0,"")
end function
%>

