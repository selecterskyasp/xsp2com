<%
'得到新闻列表 返回查询到的内容 如果没有结果,则返回 暂无相关信息
'snum		条数 
'sfields 	字段(已存在SMT_id,SMT_title,SMT_htmlurl,SMT_date如果有新增则将字段加在后面) 
'swhere		条件(已存在SMT_key=1 and SMT_key1=1 and SMT_key2=1)
'ssort 		排序方式(默认SMT_date desc) 
'sm    		模式 0为普通(只带标题) 1为带日期和标题 2为新闻列表
function getNewsInfo(snum,sfields,swhere,ssort,sm)
	dim getinfoRs,getinfoContent,getinfosql
	dim getinfoi
	dim getinfopage,getinfopagesize,getinforecordcount,getinfopagecount
	dim tmptitle,tmpid,tmpurl,tmpdate,tmpcontent
	dim tmpsfields,tmpswhere,tmpssort,tmpinputKeyword
	tmpsfields="SMT_id,SMT_title,SMT_htmlurl,SMT_date"
	tmpswhere="SMT_key1=1 and SMT_key2=1"
	tmpssort="SMT_date desc"
	if not chkNull(sfields,1) then sfields=tmpsfields&","&sfields else sfields=tmpsfields
	if not chkNull(swhere,1) then swhere=tmpswhere&" and "&swhere else swhere=tmpswhere
	if chkNull(ssort,1) then ssort=tmpssort
	if sm=2 then
		getinfopage=trim(request.querystring("page"))		
		if chkrequest(getinfopage) then getinfopage=clng(getinfopage) else getinfopage=1
		sql="select count(SMT_id) from SMT_xxnews where  "&swhere&""
		getinforecordcount=getdbvalue(sql,1)(1)
  		getinfopagecount=Abs(Int(getinforecordcount/snum*(-1))) 
		if getinfopagecount=0 then getinfopagecount=1
		if getinfopage>getinfopagecount and getinfopagecount>1 then getinfopage=getinfopagecount
		if getinfopage=1 then
			getinfosql="select top "&snum&" "&sfields&" from SMT_xxnews where "&swhere&" order by "&ssort
		else
			getinfosql="SELECT TOP "&snum&" "&sfields&" from SMT_xxnews  where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((getinfopage-1)*snum)&" SMT_id FROM SMT_xxnews where "&swhere&" order by "&ssort&") AS tblTMP)) and "&swhere&" order by "&ssort&""  			
		end if
	else		
		getinfosql="select top "&snum&" "&sfields&" from SMT_xxnews where "&swhere&" order by "&ssort
	end if
	
	tmparr=getdbvaluelist(getinfosql)
'	response.write(getinfoSql)
'	response.end
	
	if clng(tmparr(0,0))=0 then
		getNewsInfo="暂无相关信息"
		exit function
	end if
	
	getinfoi=1
	getinfoContent=""
	getinfoContent=getinfoContent&""&vbcrlf
	if sm=2 then getinfoContent=getinfoContent&"<ul class=""newsmorelist"">"&vbcrlf
	for i=0 to ubound(tmparr,2)
		tmpid=tmparr(0,i)
		tmpurl=tmparr(2,i)&tmpid&".html"
		tmpdate=tmparr(3,i)
		select case sm
			case 0:
				tmpTitle=leftt(tmparr(1,i),15)				
				getinfoContent=getinfoContent&"<li>·<a href="""&tmpurl&""" target=""_blank"">"&tmpTitle&"</a></li>"&vbcrlf
			case 1:
				tmpTitle=leftt(tmparr(1,i),15)	
				getinfoContent=getinfoContent&"<li>·<a href="""&tmpurl&""" target=""_blank"">"&tmpTitle&"</a> ["&formatdatetime(tmpdate,2)&"]</li>"&vbcrlf
			case 2:
				tmpinputKeyword=checkstr(request.QueryString("keyword"))
				tmpTitle=leftt(tmparr(1,i),15)
				tmpcontent=leftt(tmparr(4,i),80)
				if not chkNull(tmpinputKeyword,2) then
				tmpTitle=replace(tmpTitle,tmpinputKeyword,"<font color=""#ff0000"">"&tmpinputKeyword&"</font>")
				tmpcontent=replace(tmpcontent,tmpinputKeyword,"<font color=""#ff0000"">"&tmpinputKeyword&"</font>")
				end if				
				getinfoContent=getinfoContent&"<li>"&vbcrlf	
				getinfoContent=getinfoContent&"<h2><a href="""&tmpurl&""" target=""_blank"">"&tmpTitle&"</a></h2>"&vbcrlf	
				getinfoContent=getinfoContent&"&nbsp;&nbsp;<span class=""gray"">"&tmpdate&"</span>"&vbcrlf	
				getinfoContent=getinfoContent&"<div style=""margin-top:5px"">"&tmpcontent&"...  <a href="""&tmpurl&""" target=""_blank""><阅读全文></a></div>"&vbcrlf	
				getinfoContent=getinfoContent&"</li>"&vbcrlf	

				getinfoContent=getinfoContent&""&vbcrlf			
		end select
	next
	if sm=2 then
	getinfoContent=getinfoContent&"</ul><div class=""pages"">"&vbcrlf	
	getinfoContent=getinfoContent&showpage(getinfopagecount,snum,getinfopage,getinforecordcount,10)
	getinfoContent=getinfoContent&"</div>"&vbcrlf	
	end if
	getNewsInfo=getinfoContent
	erase tmparr:getinfoContent=""
end function

'得到行业分类列表 swhere条件 scount当前分类下共有多少条记录 sbid大分类ID sbname大分类名称 ssid小分类ID ssname小分类名称 skeyword关键字 smode模式 0为求购 1为供应 2为企业大全
function getflNav(swhere,scount,sbid,sbname,ssid,ssname,skeyword,smode)
dim grs,gsql,gsqlcout,gsqlcout1
dim tcontent,tcount
dim tmodename,tlink,ttable,tpara,tpos1,tpos2
tlink=""
select case smode
case 0
	tmodename="找求购"
	'tlink="/buy/"
	gsqlcout="select count(SMT_id) from SMT_sca where SMT_key=1 and SMT_key1=1 and SMT_key2=1 "&swhere
case 1
	tmodename="找产品"
	'tlink="/sell/"
	gsqlcout="select count(SMT_id) from SMT_cp where SMT_key=1 and SMT_newpro<>1 and SMT_key1=1 and SMT_key2=1 "&swhere
case 2
	cojy=request.QueryString("cojy")
	'tlink="/company/"
	select case cojy
	case "1"
		tmodename="采购商"	
	case "2"
		tmodename="中国供应商"
	case "3"
		tmodename="市场经销商"	
	case else
		tmodename="企业大全"		
	end select
	
	gsqlcout="select count(SMT_id) from SMT_yp where SMT_flag=0 and (SMT_key=1 or SMT_key=3) and smt_key1=1 "&swhere
end select

tcontent=tcontent&"<div class=""column"">"&vbcrlf
tcontent=tcontent&"<div class=""columntitle"">&nbsp;当前位置：<a href=""http://www.xsp2.com"">首页</a> &gt; <a href="""&tlink&""">"&tmodename&"</a> &gt; "

if sbid<>0 then 	
	tcontent=tcontent&"<a href="""&tlink&"?bid="&sbid&""">"&sbname&"</a> &gt; "	
	if ssid<>0 then		
			tcontent=tcontent&"<a href="""&tlink&"?bid="&sbid&"&sid="&ssid&""">"&ssname&"</a> &gt; "	
	end if
	gsql="select SMT_ypxxtwo,SMT_ypxxtwo_id from SMT_ypxxtwo where SMT_ypxxone_id="&sbid
else
	gsql="select SMT_ypxxone,SMT_ypxxone_id from SMT_ypxxone"
end if
if chkRange(keyword,1,30) then
tcontent=tcontent&"搜索关于 <font color=red>"&skeyword&"</font> 的信息&nbsp;&nbsp;"
end if
tcontent=tcontent&"当前分类下共有(<span class=""red""><strong>"&scount&"</strong></span>)条信息</div>"&vbcrlf
tcontent=tcontent&"<div class=""columncenter""><div class=""categorylist""><ul>"

set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1

if grs.bof and grs.eof then
	tcontent=tcontent&"<li><a href=""javascript:;"">暂无小类</a></li>"&vbcrlf
else
	tpara=Request.ServerVariables("QUERY_STRING")	
	tpara=delUrlpara(tpara,"bid,sid,submit")	
	do while not grs.eof
		ssname=trim(grs(0))
		ssid=trim(grs(1))		
		if sbid<>0 then
		tcontent=tcontent&"<li><a href="""&tlink&tpara&"bid="&sbid&"&sid="&ssid&""">"&ssname&"</a>"
		tpos1=instr(gsqlcout,"SMT_ypxxtwo_id=")
		if tpos1>1 then
			tpos2=instr(tpos1,gsqlcout," ")
			if tpos2>1 then
				gsqlcout1=left(gsqlcout,tpos1+len("SMT_ypxxtwo_id=")-1)&ssid&" "&right(gsqlcout,len(gsqlcout)-tpos2)
			else
				gsqlcout1=left(gsqlcout,tpos1+len("SMT_ypxxtwo_id=")-1)&ssid
			end if	
		else
			gsqlcout1=gsqlcout&" and SMT_ypxxtwo_id="&ssid
		end if		
		
		'tcount=conn.execute(gsqlcout1)(0)
		'tcontent=tcontent&"<span class=""gray"">("&tcount&")</span>"
		tcontent=tcontent&"</li>"&vbcrlf
		else
		tcontent=tcontent&"<li><a href="""&tlink&tpara&"bid="&ssid&""">"&ssname&"</a>"
		gsqlcout1=gsqlcout&" and SMT_ypxxone_id="&ssid		
		'tcount=conn.execute(gsqlcout1)(0)
		'tcontent=tcontent&"<span class=""gray"">("&tcount&")</span>"
		tcontent=tcontent&"</li>"&vbcrlf
		end if
		grs.movenext
	loop
end if
tcontent=tcontent&"</ul></div>"&vbcrlf
tcontent=tcontent&"</div></div>"&vbcrlf
grs.close	
set grs=nothing
getflNav=tcontent
end function

'得到供应求购公司列表 swhere 条件 sorder 排序方式默认为SMT_id desc smode 0为求购 1为供应 2为公司
function getList(swhere,sorder,smode)
dim grs,gsql,grs1
dim tcontent,tpage,tp,tfields,ttable,twhere,torder
dim tlink,tlink1,tlink2
dim tvip,tvippic,tuser,tpic,ttitle,tjj,taddonename,taddtwoname,tqq,ti,tkeyword

select case smode
case 0
	tlink="http://www.xsp2.com/buy/detail/"
	tfields="SMT_id,SMT_scatitle,SMT_sca,SMT_addtwo_id,SMT_begindate,SMT_picture,SMT_yp_id"
	ttable="SMT_sca"
	twhere="SMT_key=1 and SMT_key1=1 and SMT_key2=1"&swhere	
case 1	
	tlink="http://www.xsp2.com/sell/detail/"
	tfields="SMT_id,SMT_cpname,SMT_cpjm,SMT_addtwo_id,SMT_date,SMT_pic,SMT_yp_id"
	ttable="SMT_cp"
	twhere="SMT_key=1 and SMT_key1=1 and SMT_key2=1 and (SMT_newpro=0 or SMT_isSetPass=0) "&swhere	
case 2	
	tlink=".xsp2.com"
	tfields="SMT_id,SMT_coname,SMT_coms,SMT_addtwo_id,SMT_date,SMT_user,SMT_vip,SMT_qqnum,SMT_cojy"
	ttable="SMT_yp"
	twhere="SMT_flag=0 and (SMT_key=1 or SMT_key=3) and smt_key1=1"&swhere	
end select

tpage=request.QueryString("page")
tp=request.QueryString("p")
if chkrequest(tpage) then tpage=cint(tpage) else tpage=1	
if tp<>"20" and tp<>"40" and tp<>"60" then tp=20 else tp=cint(tp)
torder="SMT_id desc"
if not chkNull(sorder,1) then torder=sorder
if tpage=1 then
	gsql="select top "&tp&" "&tfields&" from "&ttable&" where "&twhere&" order by "&torder
else
	gsql="SELECT TOP "&tp&" "&tfields&" from "&ttable&"  where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((tpage-1)*tp)&" SMT_id FROM "&ttable&" where "&twhere&" order by SMT_id desc) AS tblTMP)) and "&twhere&" order by "&torder&""  			
end if

tmparr=getdbvaluelist(gsql)

if clng(tmparr(0,0))=0 then
getList="暂无相关信息"
exit function
end if
ti=1
tcontent=""
tcontent=tcontent&""&vbcrlf

for i=0 to ubound(tmparr,2)
	if smode=2 then
		tvip=tmparr(6,i)
		tuser=tmparr(5,i)
	else		
		gsql="select top 1 SMT_vip,SMT_user from SMT_yp where SMT_id="&trim(tmparr(6,i))	
		tmparr2=getdbvalue(gsql,2)		
		if tmparr2(0)=0 then
			tvip=0
			tuser="www"
		else
			tvip=cint(tmparr2(1))
			tuser=tmparr2(2)
		end if
		erase tmparr2
	end if
	select case tvip
		case 2 'VIP
		tvippic="/images/rank2.gif"
		case 4 '试用会员
		tvippic="/images/rank.gif"
		case 6 '普通会员
		tvippic="/images/rank.gif"
		case else '默认
		tvippic="/images/rank.gif"
	end select
	tpic="/images/noimg.gif"
	ttitle=leftt(tmparr(1,i),15)
	tjj=leftt(tmparr(2,i),50)
	tkeyword=checkstr(request.QueryString("keyword"))
	if chkrange(tkeyword,1,25) then
		if len(tkeyword)>3 then	tkeyword=getkeyword(tkeyword)(1)		
		ttitle=boldKeyword(ttitle,tkeyword,0)
		tjj=boldKeyword(tjj,tkeyword,0)		
	end if
	gsql="select top 1 SMT_addone,SMT_addtwo from SMT_ypaddtwo where SMT_addtwo_id="&trim(tmparr(3,i))
	
	tmparr2=getdbvalue(gsql,2)
	if tmparr2(0)<>0 then
		taddonename=tmparr2(1)
		taddtwoname=tmparr2(2)
	else
		taddonename="未知"
		taddtwoname=""
	end if
	erase tmparr2
	tcontent=tcontent&"<li class=""sellcontent1""><img src="""&tvippic&""" /></li>"&vbcrlf
	if smode<>2 then
		if not chkNull(tmparr(5,i),20) then
			tpic=trim(tmparr(5,i))
		end if				
		tlink1="http://"&tuser&".xsp2.com"&"/productdetail.asp?proID="&tmparr(0,i)&"#me"
		tlink2=tlink&tmparr(6,i)&"_"&tmparr(0,i)&".html"
		tcontent=tcontent&"<li class=""sellcontent2""><a href="""&tlink2&""" target=""_blank""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" /></a></li>"&vbcrlf		
	else			
		tlink1="http://"&tuser&".xsp2.com"&"/contact.asp"	
		tlink2="http://"&tmparr(5,i)&tlink
	end if
	tcontent=tcontent&"<li class=""sellcontent3"">"&vbcrlf
	tcontent=tcontent&"<div class=""sellname""><h2><a href="""&tlink2&""" target=""_blank"">"&ttitle&"</a></h2>"&vbcrlf
	tcontent=tcontent&"</div><div class=""time"">"&formatdatetime(trim(tmparr(4,i)),2)&"</div>"&vbcrlf
	tcontent=tcontent&"<div class=""description"">"&tjj&"...</div>"&vbcrlf
	tcontent=tcontent&"<a href="""&tlink2&""" target=""_blank"">查看详细信息</a></li>"&vbcrlf
	if smode=2 then
		tcontent=tcontent&"<li class=""sellcontent2"">"&tmparr(8,i)&"</li>"&vbcrlf
	end if
	tcontent=tcontent&"<li class=""sellcontent4"">"&taddonename&taddtwoname&"</li>"&vbcrlf
	tcontent=tcontent&"<li class=""sellcontent5""><a href="""&tlink1&"""><!--<img src=""/images/qq.gif"" width=""77"" height=""17"" border=""0"" />-->点这里给我留言</a></li>"&vbcrlf
	ti=ti+1	
next
getList=tcontent
tcontent="":erase tmparr
end function 

'得到供应求购公司列表 swhere 条件 sorder 排序方式默认为SMT_id desc smode 0为求购 1为供应 2为公司
function getCpList(swhere,sorder,smode)
dim grs,gsql,grs1
dim tcontent,tpage,tp,tfields,ttable,twhere,torder
dim tlink,tlink1,tlink2
dim tvip,tvippic,tuser,tpic,ttitle,tjj,taddonename,taddtwoname,tqq,ti,tkeyword

tlink=".xsp2.com"
tfields="SMT_id,SMT_coname,SMT_coms,SMT_addtwo_id,SMT_date,SMT_user,SMT_vip,SMT_qqnum,SMT_cojy"
ttable="SMT_yp"
twhere="SMT_flag=0 and (SMT_key=1 or SMT_key=3) and smt_key1=1"&swhere	


tpage=request.QueryString("page")
tp=request.QueryString("p")
if chkrequest(tpage) then tpage=cint(tpage) else tpage=1	
if tp<>"20" and tp<>"40" and tp<>"60" then tp=20 else tp=cint(tp)
if not chkNull(sorder,1) then torder=sorder
if tpage=1 then
	gsql="select top "&tp&" "&tfields&" from "&ttable&" where "&twhere&" order by "&torder
else
	gsql="select  "&tfields&" from "&ttable&" where "&twhere&" order by "&torder			
end if

set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not (grs.bof and grs.eof) then
	if tpage<>1 then
	grs.pagesize=tp
	grs.AbsolutePage=tpage
	end if
	do while not grs.eof and ti<tp	
	tvip=grs("SMT_vip")
	tuser=grs("SMT_user")	
	select case tvip
		case 2 'VIP
		tvippic="/images/rank2.gif"
		case 4 '试用会员
		tvippic="/images/rank.gif"
		case 6 '普通会员
		tvippic="/images/rank.gif"
		case else '默认
		tvippic="/images/rank.gif"
	end select
	tpic="/images/noimg.gif"
	ttitle=leftt(grs("SMT_coname"),15)
	tjj=leftt(grs("SMT_coms"),50)
	tkeyword=checkstr(request.QueryString("keyword"))
	if chkrange(tkeyword,1,25) then
		if len(tkeyword)>3 then tkeyword=getkeyword(tkeyword)(1)
		ttitle=boldKeyword(ttitle,tkeyword,0)
		tjj=boldKeyword(tjj,tkeyword,0)	
		'ttitle=replace(ttitle,tkeyword,"<font color=""#FF0000"">"&tkeyword&"</font>")
		'tjj=replace(tjj,tkeyword,"<font color=""#FF0000"">"&tkeyword&"</font>")
	end if
	gsql="select top 1 SMT_addone,SMT_addtwo from SMT_ypaddtwo where SMT_addtwo_id="&trim(grs("SMT_addtwo_id"))
	
	tmparr2=getdbvalue(gsql,2)
	if tmparr2(0)<>0 then
		taddonename=tmparr2(1)
		taddtwoname=tmparr2(2)
	else
		taddonename="未知"
		taddtwoname=""
	end if
	erase tmparr2
	tcontent=tcontent&"<li class=""sellcontent1""><img src="""&tvippic&""" /></li>"&vbcrlf			
	tlink1="http://"&tuser&".xsp2.com"&"/contact.asp"		
	tlink2="http://"&grs("SMT_user")&tlink	
	tcontent=tcontent&"<li class=""sellcontent3"">"&vbcrlf
	tcontent=tcontent&"<div class=""sellname""><h2><a href="""&tlink2&""" target=""_blank"">"&ttitle&"</a></h2>"&vbcrlf
	tcontent=tcontent&"</div><div class=""time"">"&formatdatetime(grs("SMT_date"),2)&"</div>"&vbcrlf
	tcontent=tcontent&"<div class=""description"">"&tjj&"...</div>"&vbcrlf
	tcontent=tcontent&"<a href="""&tlink2&""" target=""_blank"">查看详细信息</a></li>"&vbcrlf		
	tcontent=tcontent&"<li class=""sellcontent2"">"&grs("SMT_cojy")&"</li>"&vbcrlf
	tcontent=tcontent&"<li class=""sellcontent4"">"&taddonename&taddtwoname&"</li>"&vbcrlf
	tcontent=tcontent&"<li class=""sellcontent5""><a href="""&tlink1&"""><!--<img src=""/images/qq.gif"" width=""77"" height=""17"" border=""0"" />-->点这里给我留言</a></li>"&vbcrlf
		ti=ti+1
		grs.movenext
	loop
else
	getCPList="暂无相关信息"
	exit function
end if
grs.close
set grs=nothing
getCPList=tcontent
tcontent=""
end function 

function getCity()
dim grs,gsql,grs1
dim tcontent
set grs=server.CreateObject("adodb.recordset")
set grs1=server.CreateObject("adodb.recordset")
sql="select SMT_addone_id,SMT_addone from SMT_ypaddone order by SMT_date desc"
tmparr=getdbvaluelist(sql)
if clng(tmparr(0,0))=0 then
getCity="暂无城市信息"
exit function
end if
tcontent=""
tcontent=tcontent&""&vbcrlf

for i=0 to ubound(tmparr,2)
	tcontent=tcontent&"<li><a href=""?province="&tmparr(0,i)&""">"&tmparr(1,i)&"</a>"&vbcrlf
	tcontent=tcontent&"<ul>"&vbcrlf
	tcontent=tcontent&"<li><div class=""areacontent"">"
	sql="select SMT_addtwo_id,SMT_addtwo from SMT_ypaddtwo where SMT_addone_id="&tmparr(0,i)&" order by SMT_date desc"
	tmparr2=getdbvaluelist(sql)
	if clng(tmparr2(0,0))=0 then
		tcontent=tcontent&"·<a href=""javascript:;"" class=""blue"">暂无城市</a>"&vbcrlf
	else
		for j=0 to ubound(tmparr2,2)
			tcontent=tcontent&"·<a href=""?province="&tmparr(0,i)&"&city="&tmparr2(0,j)&""" class=""blue"">"&tmparr2(1,j)&"</a>"&vbcrlf
		next
	end if
	erase tmparr2
	tcontent=tcontent&"</div></li>"
	tcontent=tcontent&"</ul></li>"
next
getCity=tcontent
tcontent="":erase tmparr
end function
%>
