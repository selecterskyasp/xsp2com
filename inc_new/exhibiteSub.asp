<%
'mode
function tmode()
dim grs,gsql,tcontent,ti
dim turl,tt
ti=1
tcontent=""
gsql=""
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=0
	tcontent=tcontent&""&vbcrlf
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
tmode=tcontent
end function

'获取热点展会新闻列表
function getHotNews()
dim grs,gsql,tcontent,ti
dim tid,tshowid
dim turl,tt
ti=1
tshowid="0"
tcontent="<div style=""margin:9px 0;padding-bottom:6px;border-bottom:1px dashed #cccccc;text-align:center"">"&vbcrlf
gsql="select top 1 smt_id,smt_title,smt_htmlurl from smt_xxnews where smt_key1=1 and smt_key=1 and smt_key2=1 and SMT_newssort=17 order by smt_date desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not grs.eof then
	tt=grs("SMT_title")
	tid=grs("SMT_id")
	turl=grs("SMT_htmlurl")&tid&".html"	
	tshowid=tshowid&","&tid
	tcontent=tcontent&"<h2><a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,15)&"</a></h2>"&vbcrlf
else
	tcontent=tcontent&"<h2><a href=""javascript:;"" >暂无相关信息</a></h2>"&vbcrlf	
end if
grs.close
gsql="select top 1 smt_id,smt_title,smt_htmlurl from smt_xxnews where smt_key1=1 and smt_key=1 and smt_key2=1 and SMT_newssort=17 and SMT_id not in("&tshowid&") order by smt_date desc"
'response.Write(gsql)
'response.Flush()
grs.open gsql,conn,1,1
if not grs.eof then
	tt=grs("SMT_title")
	tid=grs("SMT_id")
	turl=grs("SMT_htmlurl")&tid&".html"	
	tshowid=tshowid&","&tid
	tcontent=tcontent&"<span class=""blue"">[最新资讯&nbsp;&nbsp;&nbsp;&nbsp;<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,14)&"</a>]</span>"&vbcrlf
else
	tcontent=tcontent&"<span class=""blue"">[最新资讯&nbsp;&nbsp;&nbsp;&nbsp;<a href=""javascript:;"" class=""blue"">暂无</a>]</span>"&vbcrlf
end if
grs.close
tcontent=tcontent&"</div><ul>"&vbcrlf

gsql="select top 4 smt_id,smt_title,smt_htmlurl from smt_xxnews where smt_key1=1 and smt_key=1 and smt_key2=1 and SMT_newssort=17 and SMT_id not in ("&tshowid&") order by smt_date desc"
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=4
	tt=grs("SMT_title")
	tid=grs("SMT_id")
	turl=grs("SMT_htmlurl")&tid&".html"	
	tcontent=tcontent&"<li>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,14)&"</a></lit>"&vbcrlf
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
getHotNews=tcontent
end function

'smode 0前面最新新闻列表 1展会新闻列表
function getNews(byVal snum,byVal sWhere,byVal smode)
dim grs,gsql,tcontent,ti
dim turl,tt,tid
dim tpage
ti=1
tcontent=""
if smode=0 then
gsql="select top "&snum&" SMT_title,SMT_htmlurl,SMT_id from SMT_xxnews where SMT_key=1 and SMT_key1=1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"
else
	tpage=request.QueryString("page")
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=1
	if tpage=1 then
		gsql="select top "&snum&" SMT_title,SMT_htmlurl,SMT_id,SMT_date,SMT_news from SMT_xxnews where SMT_key=1 and SMT_key1=1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"
	else
		gsql="SELECT TOP "&snum&" SMT_title,SMT_htmlurl,SMT_id,SMT_date,SMT_news from SMT_xxnews where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((tpage-1)*snum)&" SMT_id FROM SMT_xxnews where SMT_key=1 and SMT_key1=1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc) AS tblTMP)) and SMT_key=1 and SMT_key1=1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"  			
	end if
end if
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum
	tid=grs("SMT_id")
	tt=grs("SMT_title")
	turl=grs("SMT_htmlurl")&tid&".html"
	if smode=0 then
		tcontent=tcontent&"<li>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,15)&"</a></li>"&vbcrlf
	else
		tcontent=tcontent&"<li>"&vbcrlf
		tcontent=tcontent&"<h2><a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,60)&"</a></h2>"&vbcrlf
		tcontent=tcontent&"&nbsp;&nbsp;<span class=""gray"">"&grs("SMT_date")&"</span>"&vbcrlf
		tcontent=tcontent&"<div style=""margin-top:5px"">"&leftt(grs("SMT_news"),80)&"...  <a href="""&turl&""" title="""&tt&""" target=""_blank""><阅读全文></a></div>"&vbcrlf
		tcontent=tcontent&"</li>"&vbcrlf
	end if	
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
getNews=tcontent
end function

'获取展会
'smode 0首页推荐展会 1普通即不带日期li 2带日期li 3展会分页列表
function getExhibite(byVal snum,byVal sWhere,byVal smode)
dim grs,gsql,tcontent,ti
dim turl,tt,tt2,tpic,tid,tdate
ti=1
tcontent=""
select case smode
case 0
gsql="select top "&snum&" SMT_z_name,SMT_htmlurl,SMT_id,SMT_z_begindate,SMT_z_lastdate,smt_pic,SMT_z_zgname from SMT_exhibit where SMT_key=2 and SMT_key2=1 order by SMT_id desc"
case 1
gsql="select top "&snum&" SMT_z_name,SMT_htmlurl,SMT_id from SMT_exhibit where SMT_key>0 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"
case 2
gsql="select top "&snum&" SMT_z_name,SMT_htmlurl,SMT_id,SMT_z_date from SMT_exhibit where SMT_key>0 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"
case 3
	tpage=request.QueryString("page")
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=1
	if tpage=1 then
		gsql="select top "&snum&" SMT_z_name,SMT_htmlurl,SMT_id,SMT_z_begindate,SMT_z_lastdate,SMT_z_zgname,SMT_z_lr from SMT_exhibit where SMT_key>0 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"
	else
		gsql="SELECT TOP "&snum&" SMT_z_name,SMT_htmlurl,SMT_id,SMT_z_begindate,SMT_z_lastdate,SMT_z_zgname,SMT_z_lr from SMT_exhibit where (SMT_id <(SELECT MIN(SMT_id) FROM (SELECT TOP "&((tpage-1)*snum)&" SMT_id FROM SMT_exhibit where SMT_key>1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc) AS tblTMP)) and SMT_key>1 and SMT_key2=1 and "&sWhere&" order by SMT_id desc"  			
	end if
end select

set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum
	tid=grs("SMT_id")
	turl=grs("SMT_htmlurl")&tid&".html"
	tt=grs("SMT_z_name")
	select case smode
	case 0
		tpic=grs("SMT_pic")
		tdate=grs("SMT_z_lastdate")
		tdate=day(tdate)&"日"
		if chkNull(tpic,1) then tpic="/images/noimg2.gif"
		tcontent=tcontent&"<li class=""leftimg""><a href="""&turl&""" title="""&tt&""" target=""_blank""><img src="""&tpic&""" border=""0"" class=""img"" alt="""&tt&""" width=""56"" height=""56""/></a></li>"&vbcrlf
		tcontent=tcontent&"<li class=""rightcon""><a href="""&turl&""" title="""&tt&""" target=""_blank""<h3>"&left(tt,15)&"</h3>"&formatdatetime(grs("SMT_z_begindate"),1)&"-"&tdate&" "&left(grs("SMT_z_zgname"),7)&"</a></li>"&vbcrlf
	case 1
		tcontent=tcontent&"<li>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,18)&"</a></li>"&vbcrlf
	case 2
		tcontent=tcontent&"<li>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,35)&"</a></li> <span class=""gray"">["&formatdatetime(grs("SMT_z_date"),2)&"]</span></li>"&vbcrlf
	case 3
		tt2=tt
		if chkrange(tk,2,100) then
		tt2=replace(tt2,tk,"<font color=""#FF0000"">"&tk&"</font>")
		end if
		tcontent=tcontent&"<li class=""morelist12"">"&vbcrlf
		tcontent=tcontent&"<h2><a href="""&turl&""" title="""&tt&""" target=""_blank"" class=""blue"">"&tt2&"</a></h2>"&vbcrlf
		tcontent=tcontent&"<div style=""margin-top:5px"">"&leftt(grs("SMT_z_lr"),80)&"</div>"&vbcrlf
		tcontent=tcontent&"<div><a href="""&turl&""" title="""&tt&""" target=""_blank""><img src=""/images/zhxq.gif"" border=""0"" /></a> <a href="""&turl&""" title="""&tt&""" target=""_blank""><img src=""/images/lxfs.gif"" border=""0"" /></a></div>"&vbcrlf
		tcontent=tcontent&"</li>"&vbcrlf
		tcontent=tcontent&"<li class=""morelist22"">"&left(grs("SMT_z_zgname"),8)&"</li>"&vbcrlf
		tcontent=tcontent&"<li class=""morelist32"">"&formatdatetime(grs("SMT_z_begindate"),2)&" <br />至 <br />"&formatdatetime(grs("SMT_z_lastdate"),2)&"</li>"&vbcrlf
	end select	
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"&vbcrlf
end if
grs.close
set grs=nothing
getExhibite=tcontent

end function
%>
