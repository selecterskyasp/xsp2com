﻿<%
'On Error Resume Next
'Server.ScriptTimeOut=9999999
function makefilename(byVal fname)
	  fname = now()
	  fname = replace(fname,"-","")
	  fname = replace(fname," ","") 
	  fname = replace(fname,":","")
	  fname = replace(fname,"PM","")
	  fname = replace(fname,"AM","")
	  fname = replace(fname,"上午","")
	  fname = replace(fname,"下午","")
	  makefilename=fname
end function 
function GetExtendName(byVal FileName)
	dim ExtName	
	ExtName = right(FileName,len(FileName)-instrrev(FileName,"."))	
	GetExtendName = ExtName
end function

'采用二进制数据流操作,为了解决编码问题生成静态页面
function SaveToFile(ByVal strBody,ByVal sFile)
	On Error Resume Next
    Dim objStream,tfile  
	tfile=server.MapPath(sfile)	
	if err then
		SaveToFile="错误ID:"&err.number&",错误描述:"&Err.Description		
        Err.Clear
		exit function	
	end if	
    Set objStream = Server.CreateObject("ADODB"+"."+"Stream")
    If Err.Number=-2147221005 Then 
        Response.Write "<div align='center'>非常遗憾,您的主机不支持ADODB.Stream,不能使用本程序</div>"
		set objStream=nothing
        Err.Clear
        Response.End
    End If		
    With objStream
        .Type = 2
        .mode = 3
        .Charset = "utf-8"
		.Open
        .Position = objStream.Size
        .WriteText = trim(strBody)
        .SaveToFile tfile,2	
		.flush
        .Close			
    End With
	if err then
		SaveToFile="错误ID:"&err.number&",错误描述:"&Err.Description	
	else
		SaveToFile="0"	
	end if		
    Set objStream = Nothing
End function

function ReadFromUTF (byVal TempString,byVal charset)    'TempString要读取的模板文件路径; Charset是编码
	On Error Resume Next
	dim str,tfilepath
	tfilepath=server.MapPath(TempString)	
	if err then
		ReadFromUTF="错误ID:"&err.number&",错误描述:"&Err.Description
		err.clear
		exit function
	end if  
    set stm=server.CreateObject("adodb.stream")
    stm.Type=2 
    stm.mode=3 
    stm.charset=charset
    stm.open
    stm.loadfromfile tfilepath
	str=stm.readtext
    stm.Close
    set stm=nothing
	if err then
		ReadFromUTF="错误ID:"&err.number&",错误描述:"&Err.Description
	else
		ReadFromUTF=str	
	end if        
end function 

'在指定的位置创建文件夹
function Createfol(byVal folpath)
	On Error Resume Next
	dim fso
	if right(folpath,1)<>"/" then folpath=folpath&"/"
	folpath=server.MapPath(folpath)	
	if err then
		Createfol="错误ID:"&err.number&",错误描述:"&Err.Description
		err.clear
		exit function
	end if 
	set fso=server.CreateObject("Scripting.FileSystemObject")	
	if err then
		Createfol="错误ID:"&err.number&",错误描述:"&Err.Description		
		err.clear
		exit function
	end if	
	if not fso.FolderExists(folpath) then		
		fso.CreateFolder(folpath)
		if err then
			Createfol="错误ID:"&err.number&",错误描述:"&Err.Description			
		else
			Createfol="0"
		end if
	else
		Createfol="文件夹已经存在"
	end if		
	set fso=nothing
end function

'删除指定的文件夹
function Delfol(byVal folpath)
	dim fsobj
	On Error Resume Next
	folpath=server.MapPath(folpath)
	if err then
		Delfol="错误ID:"&err.number&",错误描述:"&Err.Description		
		err.clear
		exit function
	end if	
	set fsobj=server.CreateObject("Scripting.FileSystemObject")	
	if  fsobj.FolderExists(folpath) then		
		fsobj.DeleteFolder(folpath)
		if err then
			Delfol="错误ID:"&err.number&",错误描述:"&Err.Description				
		else
			Delfol="0"
		end if	
	else
		Delfol="文件夹不存在"
	end if	
	set fsobj=nothing
end function

'删除指定的文件
function DelFile(byVal filename)
	On Error Resume Next
	dim delobj
	set delobj=server.CreateObject("Scripting.FileSystemObject")
	
	filename=server.MapPath(filename)	
	if err then
		DelFile="错误ID:"&err.number&",错误描述:"&Err.Description&",自定义:路径格式不正确"
		err.clear
		exit function
	end if	
	if delobj.FileExists(filename) then
		delobj.DeleteFile(filename)
		if err then
			DelFile="错误ID:"&err.number&",错误描述："&Err.Description&",自定义:可能没有删除权限"
			err.clear
			exit function
		else
			DelFile="0"
		end if	
	else
		delFile="文件不存在"	
	end if
	set delobj=nothing	
end function

'判断文件是否存在
Function isExists(byVal FileName) 
On Error Resume Next
dim delobj
filename=server.MapPath(filename)
if err then
	isExists=false
	err.clear
	exit function
end if	
set delobj=server.CreateObject("Scripting.FileSystemObject")
if delobj.FileExists(filename) then
	isExists=true
else
	isExists=false
end if
set delobj=nothing
End Function 

'判断指定文件夹是否为空
function isNullFolder(byVal path)
 	dim fs,f,f1,fc,s,folderPath
	On Error Resume Next
	folderPath=server.mapPath(path)
	if err then
		isNullFolder=true
		err.clear
		exit function
	end if
	set fs=createobject("scripting.filesystemobject") 	
	if fs.FolderExists(folderPath) then
	set f=fs.GetFolder(folderPath)
	if f.size=0 then
	isNullFolder=true
	else
	isNullFolder=false
	end if
	set f=nothing
	else
	isNullFolder=false
	end if 
	set fs=nothing
end function
%>