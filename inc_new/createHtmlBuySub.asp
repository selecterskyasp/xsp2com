
<%
'本页面需要包含conn.asp safe_info.asp funciton.asp head.asp
'创建求购信息静态页面
function createBuyDetail(byVal sid)
dim gStr,gTitle,gDes,gKeyword
dim gRs,gSql,gRs1
dim template,gUsername
dim ypxxone,ypxxtwo,ypid
dim nextid,preid
nextid=0
preid=0
sid=trim(sid)
set gRs=server.CreateObject("adodb.recordset")
gSql="select top 1 SMT_scatitle,SMT_sca,SMT_picture,SMT_begindate,SMT_lastdate,SMT_price,SMT_minNum,SMT_price,SMT_sm,SMT_gg,SMT_keyword,SMT_yp_id,SMT_ypxxone_id,SMT_ypxxtwo_id,SMT_key,SMT_key1,SMT_key2,SMT_yp_id from SMT_sca where  SMT_id="&sid
gRs.open gSql,conn,1,1
if gRs.bof and gRs.eof then
	call closers(gRs)
	call createBuyPreNextRecord(sid)
	createBuyDetail=1
	exit function
end if
ypid=gRs("SMT_yp_id")
if cint(gRs("SMT_key"))<>1 or cint(gRs("SMT_key1"))<>1 or cint(gRs("SMT_key2"))<>1 then	
	call delfile("/buy/detail/"&ypid&"_"&sid&".html")	
	gRs.close
	set grs=nothing
	call createBuyPreNextRecord(sid)	
	createBuyDetail=2
	exit function
end if
set gRs1=server.CreateObject("adodb.recordset")
gSql="select top 1 SMT_user,SMT_key,SMT_key1 from SMT_yp where SMT_id="&gRs("SMT_yp_id")
gRs1.open gSql,conn,1,1
if gRs1.bof and gRs1.eof then
call closers(gRs)
call closers(gRs1)
createBuyDetail=3
exit function
end if
if (cint(grs1("SMT_key"))<>1 and cint(grs1("SMT_key"))<>3) or cint(grs1("SMT_key1"))<>1 then
sql="update SMT_sca set SMT_key2=0 where SMT_id="&sid
call setdbvalue(sql)
call closers(gRs)
call closers(gRs1)
createBuyDetail=3
exit function
end if
gUsername=gRs1("SMT_user")
gRs1.close

gSql="select top 1 SMT_ypxxone from SMT_ypxxone where SMT_ypxxone_id="&gRs("SMT_ypxxone_id")
gRs1.open gSql,conn,1,1
if gRs1.bof and gRs1.eof then	
	call closers(gRs)
	call closers(gRs1)
	createBuyDetail=4
	exit function
end if
ypxxone=gRs1("SMT_ypxxone")
gRs1.close

gSql="select top 1 SMT_ypxxtwo from SMT_ypxxtwo where SMT_ypxxtwo_id="&gRs("SMT_ypxxtwo_id")
gRs1.open gSql,conn,1,1
if gRs1.bof and gRs1.eof then	
	call closers(gRs)
	call closers(gRs1)
	createBuyDetail=5
exit function
end if
ypxxtwo=gRs1("SMT_ypxxtwo")
gRs1.close


gDes=leftt(gRs("SMT_sca"),100)
gTitle=getinnertext(gRs("SMT_scatitle"))&"_"&gRs("SMT_scatitle")&"批发_求购信息_大中华市场网"
gKeyword=getinnertext(gRs("SMT_keyword"))&","&gRs("SMT_scatitle")&"批发,求购信息,大中华市场网"

gStr=head(gKeyword,gDes,gTitle,"求购信息",20)&vbcrlf

gStr=gStr&nav("求购信息","/buy/","> <a href='/buy/?bid="&gRs("SMT_ypxxone_id")&"' class='blue'>"&ypxxone&"</a> > <a href='/buy/?bid="&gRs("SMT_ypxxone_id")&"&sid="&gRs("SMT_ypxxtwo_id")&"' class='blue'>"&ypxxtwo&"<a> > "&getinnertext(gRs("SMT_scatitle")))

template=ReadFromUTF("/template/buy/detail.html","utf-8")
template=replace(template,"$头部",gStr)

template=replace(template,"$标题",gRs("SMT_scatitle"))

gStr=gRs("SMT_picture")
if chkNull(gStr,10) then gStr="/images/noimg.gif"
template=replace(template,"$图片地址",gStr)

gStr=getPrice(gRs("SMT_price"),2)
if gStr=0 then gStr="面议"
template=replace(template,"$单价",gStr)

gStr=gRs("SMT_minNum")
if not chkrequest(gStr) then gStr="无限制求购"
template=replace(template,"$需求数量",gStr)
gStr=gRs("SMT_sm")
if chkNull(gStr,1) then gStr="无要求"
template=replace(template,"$包装说明",gStr)
gStr=gRs("SMT_gg")
if  chkNull(gStr,1) then gStr="无要求"
template=replace(template,"$产品规格",gStr)
template=replace(template,"$发布日期",gRs("SMT_begindate"))
template=replace(template,"$有效期",formatdatetime(gRs("SMT_lastdate"),2))
template=replace(template,"$用户名",gUsername)
template=replace(template,"$信息ID号",sid)
template=replace(template,"$信息标题",server.URLEncode(gRs("SMT_scatitle")))

'上一条
gSql="select top 1 SMT_id,SMT_yp_id from SMT_sca where SMT_key =1 and SMT_key1=1 and SMT_key2=1 and SMT_ypxxtwo_id="&gRs("SMT_ypxxtwo_id")&" and SMT_id<"&sid&" order by SMT_id desc"
gRs1.open gSql,conn,1,1
if gRs1.bof and gRs1.eof then
gStr="javascript:;"
else
gStr=gRs1("SMT_yp_id")&"_"&gRs1("SMT_id")&".html"
end if
gRs1.close
template=replace(template,"$上一条链接",gStr)

'下一条
gSql="select top 1 SMT_id,SMT_yp_id from SMT_sca where SMT_key =1 and SMT_key1=1 and SMT_key2=1 and SMT_ypxxtwo_id="&gRs("SMT_ypxxtwo_id")&" and SMT_id>"&sid&" order by SMT_id asc"
gRs1.open gSql,conn,1,1
if gRs1.bof and gRs1.eof then
gStr="javascript:;"
else
gStr=gRs1("SMT_yp_id")&"_"&gRs1("SMT_id")&".html"
end if
gRs1.close
template=replace(template,"$下一条链接",gStr)
gStr=getCoInfo(gRs("SMT_yp_id"))
template=replace(template,"$公司联系方式",gStr)
template=replace(template,"$详细信息",replace_t(gRs("SMT_sca")))
template=replace(template,"$企业ID号",gRs("SMT_yp_id"))
template=replace(template,"$行业分类ID号",gRs("SMT_ypxxtwo_id"))
call SaveToFile(template,"/buy/detail/"&gRs("SMT_yp_id")&"_"&sid&".html")
gRs.close
set gRs=nothing
set gRs1=nothing
createBuyDetail=0
end function

'生成当前求购的上下条记录
function createBuyPreNextRecord(byVal sid)
dim preid,nextid,grs,gsql
preid=0:nextid=0
gSql="select top 1 SMT_id from SMT_sca where SMT_id<"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
preid=clng(gRs("SMT_id"))
end if
gRs.close

gSql="select top 1 SMT_id from SMT_sca where SMT_id>"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 order by SMT_id asc"
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
nextid=clng(gRs("SMT_id"))
end if
gRs.close
set grs=nothing
if preid<>0 then 
	call createBuyDetail(preid)
end if
if nextid<>0 then 
	call createBuyDetail(nextid)
end if
end function

'生成当前求购的上一条记录
function createBuyPreRecord(byVal sid)
dim preid,grs,gsql
preid=0
gSql="select top 1 SMT_id from SMT_sca where SMT_id<"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
preid=clng(gRs("SMT_id"))
end if
gRs.close
set grs=nothing
if preid<>0 then 
	call createBuyDetail(preid)
end if
end function

'得到企业的联系方式
function getCoInfo(byVal scoid)
dim gRs,gContent
dim gUrl,gDjid,getDJ
set gRs=server.CreateObject("adodb.recordset")
gRs.open "select top 1 SMT_coname,SMT_user,SMT_coaddress,SMT_coyb,SMT_colxr,SMT_coxz,SMT_cojy,SMT_vip,SMT_cotelq,SMT_cotel,SMT_cofaxq,SMT_cofax from SMT_yp where SMT_id="&scoid,conn,1,1
if gRs.bof and gRs.eof then
getCoInfo="企业不存在"
exit function
end if
gUrl="http://"&gRs("SMT_user")&".xsp2.com/"
gDjid=cint(gRs("SMT_vip"))
select case gDjid
	case 1
	getDJ="VIP申请中"
	case 2
	getDJ="VIP会员"
	case 3
	getDJ="试用申请中"
	case 4
	getDJ="试用会员"
	case 5
	gDj="普通申请中"
	case 6
	getDJ="普通会员"
	case else
	getDJ="普通会员"
end select
gContent=""
gContent=gContent&""&vbcrlf
gContent=gContent&"<h2><a href="""&gUrl&""" class=""blue"" target=""_blank"">"&leftt(gRs("SMT_coname"),20)&"</a></h2>"&vbcrlf
gContent=gContent&"<div style=""margin:10px 0""><a href="""&gUrl&""" target=""_blank""><img src=""/images/btn_web.gif"" border=""0"" /></a></div>"&vbcrlf
gContent=gContent&" 地址："&gRs("SMT_coaddress")&" <br />"&vbcrlf
gContent=gContent&" 邮编："&gRs("SMT_coyb")&" <br />"&vbcrlf
gContent=gContent&" 企业类型："&gRs("SMT_coxz")&" <br />"&vbcrlf
gContent=gContent&" 经营模式："&gRs("SMT_cojy")&" <br />"&vbcrlf

gContent=gContent&" 会员等级：<span class=""blue"">"&getDJ&"</span> <br />"&vbcrlf
gContent=gContent&" 联系人：<span class=""blue"">"&gRs("SMT_colxr")&"</span> <br />"&vbcrlf
gContent=gContent&" 电话："&gRs("SMT_cotelq")&" - "&gRs("SMT_cotel")&" <br />"&vbcrlf
gContent=gContent&" 传真："&gRs("SMT_cofaxq")&" - "&gRs("SMT_cofax")&" <br />"&vbcrlf

gRs.close
set gRs=nothing
getCoInfo=gContent
end function
%>
