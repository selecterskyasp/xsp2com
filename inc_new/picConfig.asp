﻿<%
'***********************************
Function Bytes2bStr(vin)
if lenb(vin) =0 then
	Bytes2bStr = ""
	exit function
end if
''二进制转换为字符串
 Dim BytesStream,StringReturn
 set BytesStream = Server.CreateObject("ADODB.Stream")
 BytesStream.Type = 2 
 BytesStream.Open
 BytesStream.WriteText vin
 BytesStream.Position = 0
 BytesStream.Charset = "utf-8"
 BytesStream.Position = 2
 StringReturn = BytesStream.ReadText
 BytesStream.close
 set BytesStream = Nothing
 Bytes2bStr = StringReturn
End Function

Function Myrequest(fldname)
	''取表单数据
	''支持对同名表单域的读取
	dim i
	dim fldHead
	dim tmpvalue
	for i = 0 to loopcnt-1
		fldHead = fldInfo(i,0)
		if instr(lcase(fldHead),lcase(fldname))>0 then
			''表单在数组中
			''判断该表单域内容
			tmpvalue = FldInfo(i,1)
			if instr(fldHead,"filename=""")<1 then
				Tmpvalue = Bytes2bStr(tmpvalue)
				if myrequest <> "" then 
					myrequest = myrequest & "," &tmpvalue
				else
					MyRequest = tmpvalue
				end if				
			else
				myrequest = tmpvalue
			end if				
		end if
		'response.write myrequest
		
	next
	
end function
function GetFileName(fldName)
	''都取原上传文件文件名
	dim i
	dim fldHead
	dim fnpos
	for i = 0 to loopcnt-1
		fldHead = lcase(fldInfo(i,0))
		if instr(fldHead,lcase(fldName)) > 0 then
			fnpos = instr(fldHead,"filename=""")
			if fnpos < 1 then exit for
			fldHead = mid(fldHead,fnpos+10)
			''表单内容
			GetFileName = mid(fldHead,1,instr(fldHead,"""")-1)
			GetfileName = mid(GetFileName,instrRev(GetFileName,"\")+1)
			
		end if
	next	
end function
function GetContentType(fldName)
	''读取上传文件的类型
	''限定读取文件域的内容
	dim i
	dim fldHead,cpos
	for i = 0 to loopcnt - 1
		fldHead = lcase(fldInfo(i,0))
		if instr(fldHead,lcase(fldName)) > 0 and instr(fldHead,"filename=""") >0 then
			''读取contentType
			cpos = instr(fldHead,"content-type: ")
			GetContentType = mid(fldHead,cpos+14)
		end if
	next
end function

Function GetFileTypeName(Fldname)

If instr(Fldname,".") > 0 Then
GetFileTypeName = right(Fldname,len(Fldname)-instrrev(Fldname,"."))
Else
call showErrmsg("\n\n*文件名非法!","")
End If
End Function

Function IsvalidFileAr(TypeName)   '限制上传文件类型           OKAr
IsvalidFileAr = false
Dim GName
For Each GName in OKAr
If TypeName = GName Then
IsvalidFileAr = true
Exit For
End If
Next
End Function
 
Function IsvalidFile(TypeName)   '限制上传图片类型           OKArpic
IsvalidFile = false
Dim GName
For Each GName in OKArpic
If TypeName = GName Then
IsvalidFile = true
Exit For
End If
Next
End Function

%>