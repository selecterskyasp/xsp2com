<%
'CreateSellDetail为生成静态页函数
'id为变量
'本页面需要包含conn.asp safe_info.asp funciton.asp head.asp
'创建求购信息静态页面
function CreateSellDetail(byVal id)
dim modeStr
dim detailRs,Csql,rs1
dim pageTitle,pageTitleshow,bigName,bigID,smallName,smallID,province,city,userid
dim preid,nextid
preid=0:nextid=0
CreateSellDetail=0
pageTitleshow="供应信息_大中华市场网"
modeStr=ReadFromUTF("/Template/sell/detail.html","utf-8")

 id = clng(id)
 set detailRs = server.createobject("adodb.recordset")
 set rs1=server.CreateObject("adodb.recordset")
 detailRs.open "select top 1 SMT_id,SMT_cpname,SMT_ypxxone_id,SMT_ypxxtwo_id,SMT_addone_id,SMT_addtwo_id,SMT_yp_id,SMT_key,SMT_key1,SMT_key2,smt_pic,smt_cpjg,smt_cpjgdw,smt_minNum,smt_maxNum,smt_cpjl,smt_begindate,smt_lastdate,smt_dl,SMT_cpbh,SMT_cpcd,SMT_cpgg,SMT_cpsm,smt_fhqx from smt_cp where (SMT_newpro=0 or SMT_isSetPass=0) and smt_id="&id,conn,1,1
 if detailRs.eof  then  
 detailRs.close
 set detailRs=nothing
 set rs1=nothing
 call createSellPreNextRecord(id)
 CreateSellDetail=1
 exit function
 end if
 userid =  detailRs("SMT_yp_id") 
 
 if cint(detailRs("SMT_key"))<>1 or cint(detailRs("SMT_key1"))<>1 or cint(detailRs("SMT_key2"))<>1 then
	call delfile("/sell/detail/"&userid&"_"&id&".html") 
	detailRs.close
	set detailRs=nothing
	set rs1=nothing
	call createSellPreNextRecord(id)
	CreateSellDetail=2
	exit function
 end if
 
 	'该信息名称
 	pageTitle = detailRs("SMT_cpname")
	bigID = detailRs("SMT_ypxxone_id")
	smallID = detailRs("SMT_ypxxtwo_id")
	
	bigName="小商品"
	smallName="家具"
	province="浙江"
	city="义乌"
 	'该求购信息大类名称
	rs1.open "select top 1 SMT_ypxxone,SMT_ypxxtwo from SMT_ypxxtwo where SMT_ypxxtwo_id="&smallID,conn,1,1
	if not rs1.eof then
	smallName=rs1("SMT_ypxxtwo")
	bigName=rs1("SMT_ypxxone")	
	end if
	rs1.close	
	rs1.open "select top 1 SMT_addone,SMT_addtwo from SMT_ypaddtwo where SMT_addtwo_id="&detailRs("SMT_addtwo_id"),conn,1,1
	if not rs1.eof then
	province=rs1("SMT_addone")
	city=rs1("SMT_addtwo")	
	end if
	pageTitleshow="中国"&province&smallName&"批发_中国"&city&bigName&"批发_"&pageTitleshow
	rs1.close
		
	'该求购信息公司信息
	dim SMT_user,SMT_coname,SMT_coaddress,SMT_coyb,SMT_coxz,SMT_cojy,SMT_colxr,SMT_cotelq,SMT_cotel,SMT_cofaxq,SMT_cofax,SMT_vip
		Csql="select top 1  SMT_user,SMT_coname,SMT_coaddress,SMT_coyb,SMT_coxz,SMT_cojy,SMT_colxr,SMT_cotelq,SMT_cotel,SMT_cofaxq,SMT_cofax,SMT_vip,SMT_key,SMT_key1 from SMT_yp where SMT_ID="&userid
		rs1.open Csql,conn,1,1
		if rs1.eof then 		
		call closers(rs1)
		call closers(detailRs)
		CreateSellDetail=3
		exit function		
		else
		if (cint(rs1("SMT_key"))<>1 and cint(rs1("SMT_key"))<>3) or cint(rs1("SMT_key1"))<>1 then
			sql="update SMT_cp set SMT_key2=0 where SMT_id="&id
			call setdbvalue(sql)
			call closers(detailRs)
			call closers(Rs1)
			createSellDetail=3
			exit function
		end if
		SMT_user=rs1("SMT_user")
		SMT_coname=rs1("SMT_coname")
		SMT_coaddress=rs1("SMT_coaddress")
		SMT_coyb=rs1("SMT_coyb")
		SMT_coxz=rs1("SMT_coxz")		
		SMT_cojy=rs1("SMT_cojy")
		SMT_colxr=rs1("SMT_colxr")
		SMT_cotelq=rs1("SMT_cotelq")
		SMT_cotel=rs1("SMT_cotel")
		SMT_cofaxq=rs1("SMT_cofaxq")
		SMT_cofax=rs1("SMT_cofax")
		SMT_vip=rs1("SMT_vip")
		end if 
		rs1.close
		pageTitleshow=pageTitle&"_"&pageTitle&"批发_"&pageTitleshow
	dim webkeywords,webDescription,webTitle,lmTitle,content
	webkeywords		= replace(pageTitleshow,"_",",")'关键字
	webDescription	= leftt(detailRs("SMT_cpsm"),150)'描述
	webTitle		= pageTitleshow'标题
	lmTitle 		= "供应信息"'栏目
	
	'替换头部
	content = ""
	content = head(webkeywords,webDescription,webTitle,lmTitle,20)'头部
	modeStr = replace(modeStr,"$头部$",content)
	'会员ID
	modeStr = replace(modeStr,"$会员ID$",SMT_user)
	
	'导航
	content = ""
	content = nav("供应信息","/sell/","> <a href='/sell/?bid="&bigID&"' class='blue'>"&bigName&"</a> > <a href='/sell/?bid="&bigID&"&sid="&smallID&"' class='blue'>"&smallName&"</a> > "&pageTitle)'导航
	modeStr = replace(modeStr,"$导航$",content)
	'替换参数
	modeStr = replace(modeStr,"$大类ID$",bigID)
	modeStr = replace(modeStr,"$小类ID$",smallID)
	modeStr = replace(modeStr,"$产品ID$",id)
	
	'产品名称1
	content = ""
	content = server.URLEncode(pageTitle)'产品名称1
	modeStr = replace(modeStr,"$产品名称1$",content)
	'产品名称
	content = ""
	content = pageTitle'产品名称
	modeStr = replace(modeStr,"$产品名称$",content)
	
	'产品图片
	content = ""
	content = detailRs("smt_pic")
	if chkNull(content,1)  then content = "/images/noimg.gif"	
	
	modeStr = replace(modeStr,"$产品图片$",content)
	
	'产品参数
	dim price
	price=getPrice(detailRs("smt_cpjg"),2)	
	if price=0 then price="面议"
	content = ""
	content = "<li>单&nbsp;&nbsp;&nbsp;&nbsp;价：<span class=""red""><strong>"&price&" "&detailRs("smt_cpjgdw")&"</strong></span></li>"&vbcrlf
	content=content&"<li>最小起订："&detailRs("smt_minNum")&"/"&detailRs("smt_cpjl")&"</li>"&vbcrlf
	content=content&"<li>供货总量："&detailRs("smt_maxNum")&"/"&detailRs("smt_cpjl")&"</li>"&vbcrlf
	content=content&"<li>发货期限："&detailRs("smt_fhqx")&"</li>"&vbcrlf
	content=content&"<li>发布日期："&detailRs("smt_begindate")&"</li>"&vbcrlf
	content=content&"<li>有 效 期："&detailRs("smt_lastdate")&"</li>"&vbcrlf
	modeStr = replace(modeStr,"$产品参数$",content)
	
	'分页
	content = ""
	'得到上一条的产品ID,产品名称,会员ID
	preid=0:nextid=0
	dim psqlName,puserid,nsqlName,nuserid	
	rs1.open "select top 1 smt_id,smt_cpname,smt_yp_id from smt_cp where  SMT_key=1 and SMT_key1=1 and SMT_key2=1  and (SMT_newpro=0 or SMT_isSetPass=0)  and smt_id<"&id&" order by SMT_id desc",conn,1,1
	if not rs1.eof then 
		preid		= clng(rs1("smt_id"))
		psqlName	= rs1("smt_cpname")
		puserid		= rs1("smt_yp_id")
	end if
	rs1.close
	
	'得到下一条的产品ID,产品名称,会员ID
	rs1.open "select top 1 smt_id,smt_cpname,smt_yp_id from smt_cp where  SMT_key=1 and SMT_key1=1 and SMT_key2=1 and (SMT_newpro=0 or SMT_isSetPass=0)  and smt_id>"&id&" order by SMT_id asc",conn,1,1
	if not rs1.eof then 
		nextid		= clng(rs1("smt_id"))
		nsqlName	= rs1("smt_cpname")
		nuserid		= rs1("smt_yp_id")
	end if
	rs1.close
	content = "<div style='margin-top:10px'>"&vbcrlf
	'response.write nsqlid
'	response.End()
	'得到会员ID
	
	if preid<>0 then 
		content = content&"<div style='float:left'>"&vbcrlf
		content = content&"<a href='/sell/detail/"&puserid&"_"&preid&".html'><img src='/images/pre.gif' width='60' height='19' border='0' align='absmiddle' title='"&psqlName&"' alt='"&psqlName&"' /></a> </div>"&vbcrlf
	else
		content = content&"<div style='float:left'>"&vbcrlf
		content = content&"<a href='javascript:;'><img src='/images/pre.gif' width='60' height='19' border='0' align='absmiddle' /></a> </div>"&vbcrlf
	end if
	if nextid<>0 then 
		content = content&"<div style='margin-left:10px; float:left'>"&vbcrlf
		content = content&"<a href='/sell/detail/"&nuserid&"_"&nextid&".html'><img src='/images/next.gif' width='60' height='19' border='0' align='absmiddle' title='"&nsqlName&"' alt='"&nsqlName&"' /></a> </div>"&vbcrlf
	else 
		content = content&"<div style='margin-left:10px;float:left'>"&vbcrlf
		content = content&"<a href='javascript:;'><img src='/images/next.gif' width='60' height='19' border='0' align='absmiddle' /></a> </div>"&vbcrlf
		
	end if
	content = content&"</div>"
	'response.write content
'	response.End()
	modeStr = replace(modeStr,"$分页$",content)
	
	
	'公司信息
	dim vip
	if cint(SMT_vip) =2 then 
		vip = "VIP"
	else
		vip = "普通" 
	end if
	content = ""
	content = "<div class=""sellright"">"&vbcrlf
	content=content&"<h2><a href='http://"&smt_user&".xsp2.com' class='blue' target=""_blank"">"&smt_coname&"</a></h2>"&vbcrlf
	content=content&"<div style='margin:10px 0'><a href='http://"&smt_user&".xsp2.com' target=""_blank""><img src='/images/btn_web.gif' border='0' /></a></div>"&vbcrlf
	content=content&"地址："&SMT_coaddress&" <br />"&vbcrlf
	content=content&"邮编："&SMT_coyb&" <br />"&vbcrlf
	content=content&"企业类型："&SMT_coxz&"<br />"&vbcrlf
	content=content&"经营模式："&SMT_cojy&"<br />"&vbcrlf
	content=content&"会员等级：<span class='blue'>"&vip&"</span> <br />"&vbcrlf
	content=content&"联系人：<span class='blue'>"&SMT_colxr&"</span><br />"&vbcrlf
	content=content&"电话："&SMT_cotelq&"-"&SMT_cotel&" <br />"&vbcrlf
	content=content&"传真："&SMT_cofaxq&"-"&SMT_cofax&"</div>"&vbcrlf
	modeStr = replace(modeStr,"$公司信息$",content)
		
	'详细说明
	dim dl
	if detailRs("smt_dl") then 
		dl = "是"
	else
		dl = "否"
	end if
	content = ""
	content = content&"编号："&trim(detailRs("SMT_cpbh"))&"<br />"&vbcrlf
	content = content&"产地："&trim(detailRs("SMT_cpcd"))&"<br />"&vbcrlf
	content = content&"规格："&trim(detailRs("SMT_cpgg"))&"<br />"&vbcrlf
	content = content&"是否代理："&trim(dl)&"<br />"&vbcrlf
	content = content&replace_t(detailRs("SMT_cpsm"))
	modeStr = replace(modeStr,"$详细说明$",content)	

detailRs.close
set detailRs=nothing
set rs1=nothing 
SaveToFile modeStr,"/sell/detail/"&userid&"_"&id&".html"
CreateSellDetail = 0
end function 

'生成当前供应的上下条记录
function createSellPreNextRecord(byVal sid)
dim preid,nextid,grs,gsql
preid=0:nextid=0
gSql="select top 1 SMT_id from SMT_cp where SMT_id<"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 and (SMT_newpro=0 or SMT_isSetPass=0)  order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
preid=clng(gRs("SMT_id"))
end if
gRs.close

gSql="select top 1 SMT_id from SMT_cp where SMT_id>"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 and (SMT_newpro=0 or SMT_isSetPass=0)  order by SMT_id asc"
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
nextid=clng(gRs("SMT_id"))
end if
gRs.close
set grs=nothing
if preid<>0 then 
	call createSellDetail(preid)
end if
if nextid<>0 then 
	call createSellDetail(nextid)
end if
end function

'生成当前供应的上一条记录
function createSellPreRecord(byVal sid)
dim preid,grs,gsql
preid=0
gSql="select top 1 SMT_id from SMT_cp where SMT_id<"&sid&" and SMT_key=1 and SMT_key1=1 and SMT_key2=1 and (SMT_newpro=0 or SMT_isSetPass=0)  order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
gRs.open gSql,conn,1,1
if not(gRs.bof and gRs.eof) then
preid=clng(gRs("SMT_id"))
end if
gRs.close
set grs=nothing
if preid<>0 then 
	call createSellDetail(preid)
end if
end function
%>