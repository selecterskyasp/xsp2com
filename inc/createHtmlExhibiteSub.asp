﻿
<%
'展会详细页面函数
function CreateExhibiteDetail(byVal id)
	'读取展会模板
	modeStr=ReadFromUTF("/Template/exhibite/detail.html","utf-8")	
	set createIndexRs=server.createobject("adodb.recordset")
	createIndexRs.open "select top 1 * from SMT_exhibit where SMT_id="&clng(id),conn,1,1	
	if not createIndexRs.eof then	
		z_name		= createIndexRs("SMT_z_name")			'展会名称		
		z_web		= createIndexRs("SMT_z_web")			'展会网站
		addone_id	= createIndexRs("SMT_addone_id")		'展会省份
		addtwo_id	= createIndexRs("SMT_addtwo_id")		'展会城市
		z_zgname	= createIndexRs("SMT_z_zgname")		'展会地址
		z_zFY		= createIndexRs("SMT_zFy")				'展会费用
		z_lr		= createIndexRs("SMT_z_lr")			'展会范围
		z_jb		= createIndexRs("SMT_z_jb")			'主办方
		z_cb		= createIndexRs("SMT_z_cb")			'承办方
		z_xb		= createIndexRs("SMT_z_xb")			'协办方
		z_others	= createIndexRs("SMT_z_others")		'展会说明
		z_zq		= createIndexRs("SMT_z_zq")
		z_sj		= createIndexRs("SMT_z_sj")
		z_cbdate	= createIndexRs("SMT_z_cbdate")
		z_qy		= createIndexRs("SMT_z_qy")
		sort_id		= createIndexRs("SMT_sort_id")
		z_xz		= createIndexRs("SMT_z_xz")
		z_begindate	= createIndexRs("SMT_z_begindate")	'开始时间
		z_lastdate	= createIndexRs("SMT_z_lastdate")	'结束时间
		z_jzdate	= createIndexRs("SMT_z_jzdate")	
		fname		= createIndexRs("SMT_htmlURL")			'生成静态页地址
		z_keyword	= createIndexRs("SMT_keyword")		'关键字
	else
	CreateExhibiteDetail=1
	exit function
	end if
	call closers(createIndexRs)

	'得到展会类别
	className = getClassName("smt_sort","SMT_exhibit_sort","SMT_id="&clng(sort_id))
	
	'关键字,描述,标题
	webkeywords		= z_keyword'关键字
	webDescription	= z_name'描述
	webTitle		= z_name & "_展会信息_大中华市场网"'标题	
	
	'头部
	content =  head(webkeywords,webDescription,webTitle,"展会信息",20)
	modeStr = replace(modeStr,"$头部$",content) 	
	
	'导航	
	navTitle	= "展会信息"
	navLink 	= "/exhibite/"
	navOther	= " > <a href='/exhibite/exhibitlist.asp' class='blue'>"&className&"</a> > "&z_name
	content 	= nav(navTitle,navLink,navOther)
	modeStr 	= replace(modeStr,"$导航$",content)
	
	'展会名称
	modeStr 	= replace(modeStr,"$展会名称$",z_name)
	
	'发布时间
	modeStr 	= replace(modeStr,"$发布时间$",z_jzdate)
	
	'开始时间
	modeStr 	= replace(modeStr,"$开始时间$",z_begindate)
	
	'结束时间
	modeStr 	= replace(modeStr,"$结束时间$",z_lastdate)
	
	'地点
	modeStr 	= replace(modeStr,"$地点$",z_zgname)
	'主办
	modeStr 	= replace(modeStr,"$主办$",z_jb)
	'承办
	modeStr 	= replace(modeStr,"$承办$",z_cb)
	'协办
	modeStr 	= replace(modeStr,"$协办$",z_xb)
	'展品范围
	modeStr 	= replace(modeStr,"$展品范围$",z_lr)
	'参展费用
	modeStr 	= replace(modeStr,"$参展费用$",z_zFY)
	'详细内容
	modeStr 	= replace(modeStr,"$详细内容$",z_others)
	'关键词
	modeStr 	= replace(modeStr,"$关键词$",z_keyword)
	'上一条	
	preId 		= getClassName("SMT_id","SMT_exhibit","SMT_id<"&clng(id)&" order by smt_id desc")
	if not chkNull(preId,1) then
	content 	= "<a href='"&getClassName("SMT_htmlURL","SMT_exhibit","SMT_id<"&clng(id)&" order by smt_id desc")&preId&".html'>< 上一条</a>"	
	else
		content = "<a href=""javascript:void(0)"">< 上一条</a>"
	end if
	modeStr 	= replace(modeStr,"$上一条$",content)
	
	nextId 		= getClassName("SMT_id","SMT_exhibit","SMT_id>"&clng(id)&" order by smt_id asc")
	if nextId <> "" then 
		content		= "<a href='"&getClassName("SMT_htmlURL","SMT_exhibit","SMT_id>"&clng(id)&" order by smt_id asc")&nextId&".html'>下一条</a>"
	else
		content = "<a href=""javascript:void(0)"">下一条 ></a>"
	end if
	
	modeStr 	= replace(modeStr,"$下一条$",content)
	
	'
	CreateExhibiteDetail = modeStr
	fname1=fname&id&".html"
	call createfol("/html/fair")
	call createfol(fname)
	SaveToFile modeStr,fname1
	CreateExhibiteDetail=0
end function
%>