<%
'本函数页面需要包含页面 conn.asp,safe_info.asp,function.asp,sub.asp
'生成市场咨询首页函数
function createNewsIndex()
dim template,tmpHead
dim tmpString
dim tmpKeyword,tmpTitle,tmpDescription,tmpHeadname
dim tmpRs,tmpId,tmpSortid,tmpNum,tmpNum2
dim tmppic,tmpNewstitle,tmpNewsurl
set tmpRs=server.createobject("adodb.recordset")
createNewsIndex=false
tmpKeyword="小商品,小商品市场,义乌小商品市场,义乌外贸,市场招商,批发市场招商,小商品市场招商,小商品供应信息,小商品求购信息,小商品产品大全,小商品企业大全,义乌小商品批发,小商品批发指南,批发指南"
tmpTitle="市场资讯 - 小商品资源网"
tmpDescription="小商品资源网(www.xsp2.com)是全球最大的小商品门户网站,也是国内批发市场招商推广的最好平台.小商品资源网由原五金供求网,浙商投资网运行商金喜宝先生巨资,聘请IT行业一流技术精英打造而成.小商品资源网立足于义乌,依托义乌丰富的小商品资源,强大的物流配送系统,10000多名长驻义乌的国外采购商,为供应,采购商搭建最便捷,安全的网上交易平台,同是小商品资源网另一栏目-中国批发市场在线,积极为全国各地的批发市场提供小商品配送,代理采购,市场招商,市场推广等一系列服务.小商品资源网的宗旨是'打造全球第一大小商品门户网站,争做最优秀的网络服务商"
tmpHeadname="市场资讯"
tmpHead=head(tmpKeyword,tmpDescription,tmpTitle,tmpHeadname)&vbcrlf
tmpHead=tmpHead&"<div id=""menu"">"&vbcrlf
tmpHead=tmpHead&menulist(tmpHeadname,"on","class=""white""","<h1>","</h1>")&vbcrlf
tmpHead=tmpHead&Newssearch()&vbcrlf
tmpHead=tmpHead&"</div>"&vbcrlf
template=ReadFromUTF("/template/news/index.html","utf-8")
template=Replace(template,"$头部",tmpHead)

'市场新闻
tmpSortid=11
tmpString=getNewsInfo(10,"","SMT_newssort="&tmpSortid,"",0)
template=Replace(template,"$市场新闻",tmpString)

'市场明星图片
tmpSortid=32
tmpNum=1
tmpRs.open "select top 3 SMT_id,SMT_title,SMT_picurl,SMT_htmlurl from SMT_xxnews where SMT_key1=1 and SMT_pic=1 and SMT_key2=1 and SMT_key3=1 and SMT_newssort="&tmpSortid&" order by SMT_date desc",conn,1,1
if not(tmpRs.eof and tmpRs.bof) then
	do while not tmpRs.eof and tmpNum<=3
		tmppic=tmpRs("SMT_picurl")
		tmpNewsurl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
		tmpNewstitle=getinnertext(tmpRs("SMT_title"))
		if chkNull(tmppic,10) then tmppic="/images/newsimg.gif"
		tmpString="<a href="""&tmpNewsurl&""" title="""&tmpNewstitle&""" target=""_blank""><img src="""&tmppic&""" border=""0"" class=""img"" width=""106"" height=""80"" /></a>"
		template=Replace(template,"$市场明星图片"&tmpNum,tmpString)
		tmpRs.movenext
		tmpNum=tmpNum+1
	loop
end if
tmpRs.close

'如果没有推荐三个明星图片
if tmpNum<=3 then
	for i=tmpNum to 4
		tmpString="<a href=""javascript:;"" title=""暂无推荐明星图片""><img src=""/images/newsimg.gif"" border=""0"" class=""img"" width=""106"" height=""80"" /></a>"
		template=Replace(template,"$市场明星图片"&i,tmpString)
	next
end if

'市场明星文字 共12条
tmpNum=1
tmpNum2=1
tmpString=""
tmpRs.open "select top 12 SMT_id,SMT_title,SMT_htmlurl from SMT_xxnews where  SMT_key1=1 and SMT_pic<>1 and SMT_key2=1 and SMT_key3=1 and SMT_newssort="&tmpSortid&" order by SMT_date desc",conn,1,1
if not (tmpRs.bof and tmpRs.eof) then
	do while not tmpRs.eof and tmpNum<=12 and tmpNum2<=3
		tmpNewsurl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
		tmpNewstitle=leftt(tmpRs("SMT_title"),10)
		tmpString=tmpString&"<li><a href="""&tmpNewsurl&""" target=""_blank"" title="""&getinnertext(tmpRs("SMT_title"))&""">"&tmpNewstitle&"</a></li>"&vbcrlf		
		if tmpNum mod 4=0 then
		template=Replace(template,"$市场明星文字"&tmpNum2,tmpString)
		tmpNum2=tmpNum2+1
		tmpString=""
		end if	
		tmpNum=tmpNum+1		
		tmpRs.movenext		
	loop
end if
tmpRs.close
'将剩下的内容替换
if tmpNum2<=3 then
	if tmpString<>"" then
		template=Replace(template,"$市场明星文字"&tmpNum2,tmpString)
		tmpNum2=tmpNum2+1
		tmpString=""
	end if
	for i=tmpNum2 to 4
	template=Replace(template,"$市场明星文字"&tmpNum2,"")
	next
end if

'生活理财
tmpSortid=14
tmpString=getNewsInfo(10,"","SMT_newssort="&tmpSortid,"",0)
template=Replace(template,"$生活理财",tmpString)

'推荐头条 一条
tmpNewstitle="请推荐一个头条新闻"
tmpNewsurl="javascript:;"
tmpRs.open "select top 1 SMT_id,SMT_title,SMT_news,SMT_htmlurl from SMT_xxnews where  SMT_key1=1 and  SMT_key2=1 and SMT_key3=2 and SMT_newssort=36 order by SMT_date desc",conn,1,1
if not(tmpRs.bof and tmpRs.eof) then
tmpNewstitle=leftt(tmpRs("SMT_title"),28)
tmpNewsurl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
tmpString="<a href="""&tmpNewsurl&""" class=""blue"" target=""_blank"" title="""&getinnertext(tmprs("SMT_title"))&""">"&tmpNewstitle&"</a>"
template=Replace(template,"$推荐头条标题",tmpString)
tmpString=leftt(tmpRs("SMT_news"),100)&"<span class=""blue"">[<a href="""&tmpNewsurl&""" class=""blue"" target=""_blank"">查看全文</a>]</span>"
template=Replace(template,"$推荐头条描述",tmpString)
else
template=Replace(template,"$推荐头条标题",tmpString)
template=Replace(template,"$推荐头条描述","")
end if
tmpRs.close

'推荐头条 二条
tmpNewstitle=""
tmpNewsurl="javascript:;"
tmpString=""
tmpNum=1
tmpRs.open "select top 2 SMT_id,SMT_title,SMT_news,SMT_htmlurl from SMT_xxnews where  SMT_key1=1 and  SMT_key2=1 and SMT_key3=3 and SMT_newssort=36 order by SMT_date desc",conn,1,1
if not (tmpRs.bof and tmpRs.eof) then
	do while not tmpRs.eof and tmpNum<=2
		tmpNewstitle=leftt(tmpRs("SMT_title"),15)
		tmpNewsurl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
		tmpString=tmpString&"<span class=""blue"">[<a href="""&tmpNewsurl&""" class=""blue"" target=""_blank"" title="""&getinnertext(tmpRs("SMT_title"))&""">"&tmpNewstitle&"</a>]</span>"
		tmpRs.movenext
	loop
	template=Replace(template,"$推荐新闻",tmpString)
else
	template=Replace(template,"$推荐新闻","")
end if
tmpRs.close

'新闻热点
tmpSortid=36
tmpString=getNewsInfo(5,"","SMT_newssort="&tmpSortid,"",1)
template=Replace(template,"$新闻热点",tmpString)

'经济资讯
tmpSortid=13
tmpString=getNewsInfo(10,"","SMT_newssort="&tmpSortid,"",1)
template=Replace(template,"$经济资讯",tmpString)

'行业新闻
tmpSortid=15
tmpString=getNewsInfo(10,"","SMT_newssort="&tmpSortid,"",1)
template=Replace(template,"$行业新闻",tmpString)

'商务指南
tmpSortid=12
tmpString=getNewsInfo(10,"","SMT_newssort="&tmpSortid,"",1)
template=Replace(template,"$商务指南",tmpString)

call SaveToFile(template,"/news/index.html")
set tmpRs=nothing
createNewsIndex=true
end function

'生成新闻详细页面函数
function createNewsdetail(byVal sid)
dim tmpRs,tmpSql
dim tmpKeywrod,tmpDescription,tmpTitle
dim tmpNewssort,tmpNewssortid,tmpNewsurl,tmpNewstitle,tmpNewstitle1,tmpNewskeyword,tmpNewsly,tmpNewscontent,tmpNewscontent1,tmpNewsid,tmpNewsdate,tmpNewspreurl,tmpNewsnexturl
dim tmpkey,tmpkey1,tmpkey2
dim tmpString,template
dim savepath
tmpSql="select top 1 SMT_id,SMT_htmlurl,SMT_title,SMT_keyword,SMT_ly,SMT_news,SMT_newssort,SMT_date ,SMT_key1,SMT_key2 from SMT_xxnews where SMT_id="&sid
set tmpRs=server.CreateObject("adodb.recordset")
tmpRs.open tmpSql,conn,1,1
if tmpRs.bof and tmpRs.eof then
	createNewsdetail=false
	tmpRs.close
	tmpRs=nothing
	exit function
end if
tmpNewsid=tmpRs("SMT_id")
tmpNewssortid=tmpRs("SMT_newssort")
tmpNewstitle=tmpRs("SMT_title")
tmpNewstitle1=tmpNewstitle
tmpNewscontent=tmpRs("SMT_news")
tmpNewscontent1=tmpNewscontent
tmpNewsly=tmpRs("SMT_ly")
tmpNewsurl=tmpRs("SMT_htmlurl")
tmpNewsKeyword=tmpRs("SMT_keyword")
tmpNewsdate=tmpRs("SMT_date")
tmpNewsKeyword=replace(replace(tmpNewsKeyword," ",","),"|",",")
tmpKey1=cint(tmpRs("SMT_key1"))
tmpKey2=cint(tmpRs("SMT_key2"))
tmpRs.close

'如果新闻没有通过审核

if tmpKey1<>1 or tmpKey2<>1 then
call delfile(tmpNewsurl&tmpNewsid&".html")
set tmpRs=nothing
createNewsdetail=false
exit function
end if
'读取新闻分类
tmpSql="select top 1 SMT_newssort from SMT_xxnewssort where SMT_id="&tmpNewssortid
tmpRs.open tmpSql,conn,1,1
if not(tmpRs.eof and tmpRs.bof) then
tmpNewssort=tmpRs("SMT_newssort")
else
tmpNewssort="未知分类"
end if
tmpRs.close
''读取上一条 下一条新闻ID号
''上一条
'tmpSql="select top 1 SMT_id,SMT_htmlurl from SMT_xxnews where  SMT_key1=1 and SMT_key2=1 and SMT_id<"&tmpNewsid&" order by SMT_id desc"
'tmpRs.open tmpSql,conn,1,1
'if tmpRs.bof and tmpRs.eof then tmpNewspreurl=tmpNewsurl&tmpNewsid&".html" else tmpNewspreurl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
'tmpRs.close
''下一条
'tmpSql="select top 1 SMT_id,SMT_htmlurl from SMT_xxnews where  SMT_key1=1 and SMT_key2=1 and SMT_id>"&tmpNewsid&" order by SMT_id desc"
'tmpRs.open tmpSql,conn,1,1
'if tmpRs.bof and tmpRs.eof then tmpNewsnexturl=tmpNewsurl&tmpNewsid&".html" else tmpNewsnexturl=tmpRs("SMT_htmlurl")&tmpRs("SMT_id")&".html"
'tmpRs.close

set tmpRs=nothing

tmpKeyword=tmpNewsKeyword&","&getinnertext(tmpNewstitle1)&","&tmpNewssort&",市场资讯,小商品资源网"
tmpDescription=leftt(tmpnewscontent1,150)
tmpTitle=getinnertext(tmpNewstitle1)&"-"&tmpNewssort&"-市场资讯-小商品资源网"

'读取模版文件
template=ReadFromUTF("/template/news/detail.html","utf-8")

'读取头部
tmpString=head(tmpKeyword,tmpDescription,tmpTitle,"市场资讯")&vbcrlf
tmpString=tmpString&"<div id=""menu"">"&vbcrlf
tmpString=tmpString&menulist("市场资讯","on","class=""white""","<h1>","</h1>")&vbcrlf
tmpString=tmpString&Newssearch()&vbcrlf
tmpString=tmpString&"</div>"&vbcrlf

template=replace(template,"$head",tmpString)

'导航
template=replace(template,"$新闻分类ID",tmpNewssortid)
template=replace(template,"$新闻分类名",tmpNewssort)

'内容
template=replace(template,"$新闻标题",tmpNewstitle)
template=replace(template,"$日期",tmpNewsdate)
template=replace(template,"$来源",tmpNewsly)
template=replace(template,"$内容",tmpNewscontent)
template=replace(template,"$上一条",tmpNewspreurl)
template=replace(template,"$下一条",tmpNewsnexturl)
template=replace(template,"$关键字",tmpNewsid&","&tmpNewskeyword)

savepath=tmpNewsurl
call createfol("/html")
call createfol("/html/xxnews")
call createfol(savepath)
savepath=tmpNewsurl&tmpNewsid&".html"
call SaveToFile(template,savepath)
createNewsdetail=true
end function
%>

