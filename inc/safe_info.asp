﻿<%
'会员发布的各种信息过滤
Function Replace_Text(byVal fString)
If Not IsNull(fString) and not isempty(fString) and fstring<>"" Then
'fString = trim(fString)
dim regEx
set regEx=new regExp
regEx.ignoreCase=true
regEx.Global=True
'<script
regEx.pattern="<script.+?script>"
fString=regEx.replace(fString,"")
'过滤asp代码
regEx.pattern="<\x25.+?\x25>"
fString=regEx.replace(fString,"")
'过滤php代码
regEx.pattern="<\x3f.+?\x3f>"
fString=regEx.replace(fString,"")

'<ifream></ifream>
regEx.pattern="<ifream.+?ifream>"
fString=regEx.replace(fString,"")
'<frameset></frameset>
regEx.pattern="<frameset.+?frameset>"
fString=regEx.replace(fString,"")
set regEx=nothing
fString = replace(fString, chr(62), "&gt;") '>
fString = replace(fString, "<", "&lt;")
fString = replace(fString, chr(60), "&lt;") '<
fString = replace(fString, """", "&quot;")
fString = Replace(fString, CHR(34), "&quot;") '双引号过滤
fString = Replace(fString, "'", "''")	'单引号过滤
End If
Replace_Text = fString
fString=""
End Function
'转换
Function replace_T(byVal fstring)
If Not IsNull(fString) and fString<>"" and not isempty(Fstring) Then
'fString = trim(fString)
fString = Replace(fString, "&lt;", "<")
fString = Replace(fString, "&gt;", ">")
fString = Replace(fString, "&quot;", """")
fString = Replace(fString, "''", "'")
end if 
replace_T = fString
fString=""
end Function

'完全过滤html
Function getInnerText(byVal strHTML)
strHTML=trim(strHTML)
  if strHTML="" or isnull(strHTML) or isempty(strHtml) then
    getInnerText="":exit function
  end if
    Dim objRegExp
    Set objRegExp = New Regexp    
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    '取闭合的<>
    objRegExp.Pattern = "<.+?>"
    '进行替换匹配
	strHTML=objRegExp.replace(strHTML,"")
	objRegExp.Pattern = "&lt;.+?&gt;"
	strHTML=objRegExp.replace(strHTML,"")
   	Set objRegExp = Nothing
	strHTML=replace(strHTML,"&nbsp;","")
	strHTML=replace(strHTML," ","")	
	strHTML = Replace(strHTML, CHR(32), "")		'&nbsp;
	strHTML = Replace(strHTML, CHR(9), "")			'&nbsp;
	strHTML = Replace(strHTML, CHR(10), "")			'&nbsp;
	strHTML = Replace(strHTML, CHR(13), "")	
    getInnerText=strHTML
	strHTML=""
    Set objRegExp = Nothing
End Function

'获取字符串长度 取得字符串长度（汉字为2)
function getLen(str)
			Dim Rep,lens,i
			Set rep=new regexp
			rep.Global=true
			rep.IgnoreCase=true
			rep.Pattern="[\u4E00-\u9FA5\uF900-\uFA2D]"
			For each i in rep.Execute(str)
				lens=lens+1
			Next
			Set Rep=Nothing
			lens=lens + len(str)
			getLen=lens			
End Function

'取完全过滤的html的字符串数
Function LeftT(byVal str,n)
  if str="" or isnull(str) or isempty(str) then
    LeftT="":exit function
  end if
str=getInnerText(str)
If len(str)<=n/2 Then
LeftT=str
Else
Dim TStr
Dim l,t,c
Dim i
l=len(str)
if l > 0 then 
	TStr=""
	t=0
	for i=1 to l
	c=asc(mid(str,i,1))
	If c<0 then c=c+65536
	If c>255 then
	t=t+2
	Else
	t=t+1
	End If
	If t>n Then exit for
	TStr=TStr&(mid(str,i,1))
	next
	LeftT = TStr
	End If
end if
str=""
End Function

'过滤SQL非法字符
Function checkStr(byVal Str)
	Str=trim(Str)	
	if isnull(Str) or Str="" or isempty(str) then
		checkStr =""
		exit Function
	else
		Dim objRegExp							
		Set objRegExp = New Regexp    
		objRegExp.IgnoreCase = True
		objRegExp.Global = True
		'取闭合的<>
		objRegExp.Pattern = "<.+?>"
		'进行替换匹配
		Str=objRegExp.replace(Str,"")
		objRegExp.Pattern = "&lt;.+?&gt;"
		Str=objRegExp.replace(Str,"")
		Set objRegExp = Nothing		
		Str = replace(Str,"''","'")
		Str = replace(Str,"'","''")
		checkStr=Str
	end if	
	Str=""
End Function

'检测传递的参数是否为大于0的数字型
Function Chkrequest(byVal Para)
Chkrequest=False
Para=trim(Para)
dim tempNum
if isnull(para) or para="" or not isnumeric(para) or isempty(para) then exit function
tempNum=cdbl(Para)
if tempNum>0 then
 Chkrequest=True
end if
Para="":tempNum=""
End Function

'检测传递的参数是否为大于0的数字型
Function Chknumber(byVal Para)
Chknumber=False
Para=trim(Para)
dim tempNum
If not IsNull(Para) and not Para="" and IsNumeric(Para) and not isempty(para) Then
	Chknumber=true
End If
Para="":tempNum=""
End Function

'检查字符串是满足长度
Function ChkNull(byVal tempPara,strlen)
tempPara=trim(tempPara)
if isnull(tempPara) or tempPara="" or len(tempPara)<strlen then
ChkNull=True
else
ChkNull=False
end if
tempPara=""
end Function

'检查字符串满足长度的范围 如果满足，返回True 否则，返回False
function chkRange(byVal tempPara,smin,sMax)
tempPara=trim(tempPara)
if isnull(tempPara) or tempPara="" or len(tempPara)<smin or len(tempPara)>sMax then
chkRange=False
else
chkRange=True
end if
tempPara=""
end function

'检查是否汉字 true是，false否
function chkChinese(para)
	if chkNull(para,1) then 
		chkChinese=false
		exit function
	end if
	Set RegExpObj=new RegExp   
	RegExpObj.Pattern="^[\u4e00-\u9fa5]+$"   
	chkChinese=RegExpObj.test(para) 
	Set RegExpObj=nothing		
end function

'检查用户名是否合法 字母开头，4-20位的数字，字母，下划线组合
Function isValidName(str)
    Dim re
    Set re = New RegExp
    re.IgnoreCase = True
    re.Global = True
    re.Pattern = "^[A-Za-z][A-Za-z0-9_]{3,19}$"
    isValidName = re.Test(str)
    Set re = Nothing	
End Function 

'返回定向文件
function getDirect()
dim tempUrl
tempUrl=checkstr(request.Cookies("direct"))
if not chkRange(tempUrl,1,100) then
tempUrl="/"
end if
getDirect=tempUrl:tempUrl=""
end function

'********************************************
'函数名：IsValidEmail
'作  用：检查Email地址合法性
'参  数：email ----要检查的Email地址
'返回值：True  ----Email地址合法
'       False ----Email地址不合法
'********************************************
function IsValidEmail(temail)	
	dim regExpobj
	set regExpobj=new regexp
	regExpobj.IgnoreCase=True
	regExpobj.Global=true
	regExpobj.Pattern="^[\w-]{1,20}@([\w-]{1,20}\.){1,}[a-zA-Z]{2,4}$"
	IsValidEmail=regExpobj.test(temail)
	set regExpobj=nothing	
end function


'检测传递的参数是否为日期型
Function Chkrequestdate(byVal Para)
Chkrequestdate=False
If Not (IsNull(Para) Or Trim(Para)="" Or Not IsDate(Para)) Then
   Chkrequestdate=True
End If
End Function

sub insert_record(table,Parameters,values)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	call setdbvalue(sql)
end sub

sub del_record(table,Conditions)'表名,条件,返回路径
sql="delete from "&table&" where "&Conditions&""
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	call setdbvalue(sql)
end sub

sub update_record(table,Parameters,Conditions)'表名,条件,返回路径
	'response.Write("update "&table&" set "&Parameters&" where "&Conditions&"")
'response.End()
sql="update "&table&" set "&Parameters&" where "&Conditions&""
	call setdbvalue(sql)
end sub
Function select_recordset(fieldname,tablename,v_fieldname,v_fieldname_value)

	  sql_view="select "&fieldname&" from "&tablename&" where  "&v_fieldname&"="&v_fieldname_value&""
	  set rs_view=server.CreateObject("adodb.recordset")
	  rs_view.open sql_view,conn
	     if not rs_view.eof then
	        select_recordset=rs_view(fieldname)
	     end if 
	  rs_view.close
	  set rs_view=nothing
 end Function
'关闭对象
sub closers(Para)	
	if typename(Para)<>"Nothing" and isobject(Para) then
		if Para.state<>0 then Para.close	
		set Para=nothing
	end if
end sub

'释放资源
sub closeconngoto()'
	if typename(conn)<>"Nothing" and isobject(Para) then
		if conn.state<>0 then conn.close	
		set conn=nothing	
	end if
end sub
sub closeconn()'会跳转
	call closeconngoto()
	'response.Cookies("direct").domain="xsp2.com"	
	'response.Cookies("direct").path="/"
	'response.Cookies("direct")="http://"&Request.ServerVariables("HTTP_HOST")&Request.ServerVariables("URL")&"?"&Request.ServerVariables("QUERY_STRING")	
end sub

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:"&msg&"');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function

'成功信息提示
function showmsg(byVal ref)
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('操作成功!');"&ref&"</script>")	
	call closers(rs)
	call closeconngoto()
	response.End()
end function
'检查是否登录(不完全检查) true已经登录 false没有登录
function chkLogin()
	if chkNull(request.Cookies("user")("name"),4) then
	chkLogin=false
	else
	chkLogin=true
	end if
end function

'检查是否登录(完全检查)
function chkTrueLogin()
dim username,userid,pass
if not request.Cookies("user").HasKeys then
chkTrueLogin=false
exit function
end if

username=checkstr(request.Cookies("user")("name"))
userid=checkstr(request.Cookies("user")("id"))
pass=checkstr(request.Cookies("user")("pass"))

if chkNull(username,4) or not chkrequest(userid) or chkNull(pass,32) then
chkTrueLogin=false
exit function
end if
sql="select top 1 SMT_id from SMT_yp where SMT_user='"&username&"' and SMT_id="&clng(userid)&" and SMT_pass='"&pass&"'"
tmparr=getdbvalue(sql,1)
if tmparr(0)=0 then
chkTrueLogin=false
else
chkTrueLogin=true
end if
erase tmparr
end function

function MemberLogout()
response.Cookies("user").domain="xsp2.com"
response.Cookies("user").path="/"
response.Cookies("user")("name")=""
response.Cookies("user")("id")=""
response.Cookies("user")("pass")=""
response.Cookies("user")("st")=""
session("userid")=""
end function

'================================================= 
  '函数名：JmailSend 
  '作 用：用Jmail发送邮件 
  '参 数：MailTo收件人地址 Subject邮件标题 Body内容 
  '返回值：flase 发送失败　true 发送成
function JmailSend(byVal MailTo,byVal Subject,byVal Body) 
   dim JmailMsg,From,FromName,Smtp,Username,Password
  ' Body 邮件内容    
  ' MailTo 收件人Email 
  ' From 发件人Email 
  ' FromName 发件人姓名 
  ' Smtp smtp服务器 
  ' Username 邮箱用户名 
  ' Password 邮箱密码 
   From="root@xsp2.com"
   FromName="大中华市场网"
   Smtp="mail@xsp2.com"
   Username="root@xsp2.com"
   Password="wujinchen"
   
   set JmailMsg=server.createobject("jmail.message") 
   JmailMsg.charset="uft-8" 
   JmailMsg.ContentType = "text/html" 
   JmailMsg.ISOEncodeHeaders = False '是否进行ISO编码，默认为True
   JmailMsg.AddHeader "Originating-IP", Request.ServerVariables("REMOTE_ADDR")
   JmailMsg.from=From
   JmailMsg.AddRecipient MailTo 
   JmailMsg.MailDomain=Smtp      
   JmailMsg.MailServerUserName =Username
   JmailMsg.MailServerPassWord =Password     
   JmailMsg.fromname=FromName     
   JmailMsg.subject=Subject 
   JmailMsg.body=Body       
   JmailSend=JmailMsg.send("mail.xsp2.com")    
   JmailMsg.close 
   set JmailMsg=nothing    
end function 

'********************************************
'函数名：makePassword
'作  用：产生指定长度的随机数字
'参  数：maxLen ----随机数的长度
'返回值：产生的随机数
'********************************************
function   makePassword(byVal   maxLen)   
  Dim   strNewPass   
  Dim   whatsNext,   upper,   lower,   intCounter   
  Randomize      
  For   intCounter   =   1   To   maxLen   
  whatsNext   =   Int((1   -   0   +   1)   *   Rnd   +   0) 
  upper   =   57   
  lower   =   48   
  strNewPass   =   strNewPass   &   Chr(Int((upper   -   lower   +   1)   *   Rnd   +   lower))   
  Next   
  makePassword   =   strNewPass       
end   function

'查找网址是否为有效的网址，用于自动定向的cookies检测
function chkUrl(byVal url)
chkUrl=false
url=lcase(url)
if (instr(url,"xsp2.com")<1 and instr(url,"xsp2.net")<1 and instr(url,"xsp2.cn")<1) or len(url)>100 then
chkUrl=false
else
chkUrl=true
end if
end function

'得到价格 如果价格为0，刚返回面议 para价格 sdot小数位数
function getPrice(byVal para,byVal sdot)
if isnull(para) or para="" or len(para)<1 then
	getPrice="面议"
	exit function
end if
para=cdbl(para)
if para=0 then
	getPrice="面议"
	exit function
end if
para=formatnumber(para,sdot)
if left(cstr(para),1)="." then para="0"+cstr(para) else para=cstr(para)
getPrice=para
end function

'输出提示信息 
'msg 提示的信息内容 
'href 1)为空时返回上一页 2)为0时不返回,即程序继续执行 3)为1为关闭当前窗口 4)为2时，父窗口返回链接 msg信息格式：信息提示内容|打开链接(注意格式，否则会出错) 5)其它值为返回到指定的网址
'flag 是否关闭数据库对象
sub alert(msg,href,flag)
dim tmparr
select case href
case ""
href="history.back();"
case "0"
href=""
case "1"
href="window.close(this);"
case "2"
tmparr=split(msg,"|")
msg=tmparr(0)
href="parent.location.href='"&tmparr(1)&"'"
erase tmparr
case else
href="location.href='"&href&"';"
end select
response.Write("<script language=""javascript"">alert('"&msg&"');"&href&"</script>")
if flag=1 then
	call closeconn()
	response.End()
end if
msg="":flag=""
end sub

'得到价格 如果价格为0，刚返回面议 para价格 sdot小数位数
function getPrice(byval para,sdot)
if isnull(para) or para="" or len(para)<1 or not isnumeric(para) then
	getPrice=0
	exit function
end if
para=cdbl(para)
if para=0 then
	getPrice=0
	exit function
end if
para=formatnumber(para,sdot)
if left(cstr(para),1)="." then para="0"+cstr(para) else para=cstr(para)
getPrice=para
para=""
end function

'删除地址中的参数
'surl 地址
'spara 要查找的参数 多个参数以,号分隔
'返回格式： 如果长度大于2 返回?a=2& 的形式 否则 返回 ?
function delUrlpara(byVal surl,spara)
if surl="" or len(surl)<1 or isnull(surl) or isempty(surl) then
delUrlpara="?"
exit function
end if
surl=lcase(surl)
spara=lcase(spara)
dim paraArr,j
dim urlarr,tmparr,t
paraArr=split(spara,",")
Dim re
Set re = New RegExp
re.IgnoreCase = True
re.Global = True
re.Pattern = "^[A-Za-z][A-Za-z0-9_]{0,255}$"

for j=0 to ubound(paraArr)
	'如果参数可能存在
	if instr(surl,paraArr(j)&"=")>0 and not chkNull(paraArr(j),1) then
		'分离所有参数
		urlarr=split(surl,"&")	
		t=""
		for i=0 to ubound(urlarr)
			'参数及值可能有效（也许参数无效但是有=号）
			if not chkNull(urlarr(i),1) and instr(urlarr(i),"=")>0 then
				tmparr=split(urlarr(i),"=")
				'如果参数不是spara,即要删除的参数 并且是有效参数(字母开头 4-20位)，则重组参数
				if tmparr(0)<>paraArr(j) and re.test(tmparr(0))  then
					if t="" then t=tmparr(0)&"="&tmparr(1) else t=t&"&"&tmparr(0)&"="&tmparr(1)					
				end if
				erase tmparr
			end if
		next
		erase urlarr
		surl=t
	end if	
next

set re=nothing
erase paraArr
if chkNull(surl,1) then surl="?" else surl="?"&surl&"&"
delUrlpara=surl
end function

'获取远程的IP地址
Function GetIP()
Dim Temp
Temp = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
If Temp ="" or isnull(Temp) or isEmpty(Temp) Then Temp = Request.ServerVariables("REMOTE_ADDR")
If Instr(Temp,"'")>0 Then Temp="0.0.0.0"
GetIP = Temp
Temp=""
End Function

'得到类别名称
function getClassName(feild,table,wheres)'feild:字段,table:表名,wheres:条件
	sql="select top 1 "&feild&" from "&table&" where "&wheres
	tmparr=getdbvalue(sql,1)
	if tmparr(0)<>0 then	
		getClassName = tmparr(1)
	else
		getClassName = ""
	end if
	erase tmparr
end function

'---------------------数据处理---------------------------
'****************************************************
'函数名：getDbValue
'作  用：取一条记录
'参  数：Sql           传入参数 Sql语句
'       ArrayCount    传入参数 需要的字段的个数
'返回值:getDbValue      输出参数 为一个数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDbValue(Sql,ArrayCount)
	ReDim NewArray(ArrayCount)
	Dim Recordcounts,Rs,i,Comm
	Set Comm = Server.CreateObject("ADODB.Command")
	With Comm
		Comm.ActiveConnection = Conn
		Comm.CommandType = 4
		Comm.CommandText = "ColumnGet"
		Comm.Prepared  = true
		Comm.Parameters.Append .CreateParameter("Return",2,4)
		Comm.Parameters.Append .CreateParameter("@Sql",200,1,1000,Sql)
		Set Rs = .Execute	
	End With
	Rs.Close
	Recordcounts=Comm(0)
	NewArray(0)=Recordcounts
	If Recordcounts=0 Then getDbValue=NewArray:Exit Function
	Set Comm=Nothing
	Rs.Open
	For i = 0 to cint(ArrayCount-1)
		NewArray(i+1)=Rs(i)
	Next
	Rs.Close
	getDbValue=NewArray
	Erase NewArray
	Sql =""
End Function


'****************************************************
'函数名：getDBValueList
'作  用：按指定sql语句取记录集
'参  数：Sql           传入参数 Sql语句
'返回值:getDBValueList      输出参数 为一个二维数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDBValueList(Sql)

	Dim Comm,Rs
	Dim Recordcounts
	Dim ArrayRecord
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = Conn
	.CommandType = 4
	.CommandText = "Columnget"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@Sql",200,1,2000,Sql)'参数1为定义的变量 2为数据类型 3为数据宽度 4为值
	Set Rs=.Execute()
	End With
	Rs.Close 
	RecordCounts=Comm(0)
	Rs.Open	
	If RecordCounts=0 Then 
		Redim ArrayRecord(0,0):ArrayRecord(0,0)=0
	Else
		ArrayRecord=Rs.GetRows()
	end if
	Rs.Close
	getDBValueList=ArrayRecord
	erase ArrayRecord
	Sql =""
End Function

'设置数据数据库 插入数据 修改数据 删除数据
'返回值：0为不成功 或者 没有影响行数 大于0的值，是当前操作影响的行数 如果是插入操作，刚返回插入后的ID号
Function setDBValue(Sql)
	'On Error Resume Next
	Dim Comm,idnum
	dim tmparr
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = Conn
	.CommandType = 4
	.CommandText = "ColumnSet"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@Sql",200,1,2000,Sql)
	.Execute()
	End With
	if err then	
		setDBValuse=0
		err.clear		
	else
		setDBValue = Comm(0)	
	end if
	Set Comm = Nothing	
	Sql=""
End Function
%>
