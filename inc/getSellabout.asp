<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
session.codepage = 65001  
Response.CharSet = "utf-8"

'==================================================
'函数名：BytesToBstr
'作  用：将获取的源码转换为中文
'参  数：Body ------要转换的变量
'参  数：Cset ------要转换的类型
'==================================================
Function BytesToBstr(byVal Body,byVal Cset)
   Dim Objstream
   Set Objstream = Server.CreateObject("adodb.stream")
   objstream.Type = 1
   objstream.Mode =3
   objstream.Open
   objstream.Write body
   objstream.Position = 0
   objstream.Type = 2
   objstream.Charset = Cset
   BytesToBstr = objstream.ReadText 
   objstream.Close
   set objstream = nothing
End Function

'==================================================
'函数名：GetHttpPage
'作  用：获取网页源码
'参  数：HttpUrl ------网页地址
'==================================================
Function GetHttpPage(byVal HttpUrl)
   If IsNull(HttpUrl)=True Or Len(HttpUrl)<18 Or HttpUrl="$False$" Then
      GetHttpPage="$False$"
      Exit Function
   End If
   Dim Http
   Set Http=server.createobject("MSXML2.XMLHTTP")
   Http.open "GET",HttpUrl,False
   Http.Send()
   if err then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   end if
   If Http.Readystate<>4 then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   End if
   GetHTTPPage=bytesToBSTR(Http.responseBody,"utf-8")
   Set Http=Nothing
   If Err.number<>0 then
      Err.Clear
   End If
End Function

a=GetHttpPage("http://www.xsp2.cn/common/inc/getxsp2comselldetail.asp")
response.Write(a)
response.End()

'dim k
'k=checkstr(request.QueryString("id"))
''参数说明 id传递过来的参数应该是： 信息ID号,行业小分类ID号，中间以 , 分隔
'if instr(k,",")<1 then
'response.Write("暂无相关信息")
'else
'response.Write(getAboutnews(k))
'end if
'call closeconngoto()
'
'function getAboutnews(byVal sid)
'dim getinfoRs,getContent
'dim getinfoi,swhere
'dim tmpTitle,tmpUrl,tmpPic
'dim arrid:arrid=split(sid,",")
'if not chkrequest(arrid(0)) or not chkrequest(arrid(1)) then
'getAboutnews="暂无相关信息"
'exit function
'end if
'swhere="SMT_key =1 and SMT_key1=1 and SMT_key2=1 and SMT_newpro<>1 and SMT_id<>"&arrid(0)&" and SMT_ypxxtwo_ID="&arrid(1)
'set getinfoRs=server.CreateObject("adodb.recordset")
'getinfoRs.open "select top 8 SMT_id,SMT_cpname,SMT_pic,SMT_yp_id from SMT_cp where "&swhere&" order by SMT_id desc",conn,1,1
'if getinfoRs.bof and getinfoRs.eof then
'	getinfoRs.close
'	set getinfoRs=nothing
'	getAboutnews="暂无相关信息"
'	exit function	
'end if
'getinfoi=1
'getContent=""
'getContent=getContent&""&vbcrlf
'
'do while not getinfoRs.eof and getinfoi<=8
'	tmpTitle=leftt(getinfoRs("SMT_cpname"),10)
'	tmpUrl="/sell/detail/"&getinfoRs("SMT_yp_id")&"_"&getinfoRs("SMT_id")&".html"
'	tmpPic=getinfoRs("SMT_pic")		
'	if chkNull(tmpPic,10) then tmpPic="/images/noimg.gif"
'	getContent=getContent&"<li><a href="""&tmpUrl&"""><img src="""&tmpPic&""" border=""0"" class=""img"" width=""80"" height=""80""/></a> <br /><a href="""&tmpUrl&""" target=""_blank"">"&tmpTitle&"</a></li>"&vbcrlf		
'	getinfoRs.movenext
'	getinfoi=getinfoi+1
'loop
'getinfoRs.close
'set getinfoRs=nothing
'getAboutnews=getContent
'end function 
%>
