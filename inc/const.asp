﻿<!--#include file="conn.asp"-->
<!--#include file="safe_info.asp"-->
<%
if not chkApp then
set rs=server.createobject("adodb.recordset")
sql="select top 1 SMT_citysypkey,SMT_cityypkey,SMT_cityvipypkey,SMT_cityscakey,SMT_citynewskey,SMT_cityscafree,SMT_cityscayx,SMT_cityping,SMT_citysql,SMT_citypingname,SMT_citysyvipdate,SMT_cityuserp,SMT_cityuserpm,SMT_cityvipp,SMT_cityvippm,SMT_cityjysj,SMT_citycpkey,SMT_flag,SMT_webkey,SMT_OKAr,SMT_OKSize,SMT_OKArpic,SMT_OKSizepic,SMT_dnskey,SMT_dnswww,SMT_citydns from SMT_config"
rs.open sql,conn,1,1
if not rs.eof then
	Application.Lock
	Application("citysypkey")=rs("SMT_citysypkey")                          
	Application("cityypkey")=rs("SMT_cityypkey")                            '注册不要后台开通？
	Application("cityvipypkey")=rs("SMT_cityvipypkey")                      
	Application("cityscakey")=rs("SMT_cityscakey")                          '发商机后台开通？
	Application("cityscafree")=rs("SMT_cityscafree")                        '免费商机后台开通？
	Application("citynewskey")=rs("SMT_citynewskey")                        '动态新闻后台开通？
	Application("cityscayx")=rs("SMT_cityscayx")                            '免费商机有效期
	Application("cityping")=replace(replace(rs("SMT_cityping"),"‘","'"),"’","'")  '要屏蔽的字符
	application("citysql")=replace(replace(rs("SMT_citysql"),"‘","'"),"’","'")
	Application("citypingname")=rs("SMT_citypingname")                      '禁止注册的用户名,以|隔开
	application("citysyvipdate")=rs("SMT_citysyvipdate")                    'VIP试用多少天
	application("cityuserp")=rs("SMT_cityuserp")                            '普通会员收费
	application("cityuserpm")=rs("SMT_cityuserpm")                          '普通会员多少钱一年
	application("cityvipp")=rs("SMT_cityvipp")                              'VIP会员收费
	application("cityvippm")=rs("SMT_cityvippm")                            'VIP会员多少钱一年
	application("cityjysj")=rs("SMT_cityjysj")                              '在几个工作日内交齐费
	application("citycpkey")=rs("SMT_citycpkey")                            '产品是否要后台开通
	application("flag")=rs("SMT_flag")
	application("webkey")=rs("SMT_webkey")                                  '网站开关
	Application("OKAr")=rs("SMT_OKAr")
	Application("OKSize")=rs("SMT_OKSize")          '文件上传
	Application("OKArpic")=rs("SMT_OKArpic")
	Application("OKSizepic")=rs("SMT_OKSizepic")    '图片上传
	application("dnskey")=rs("SMT_dnskey")                                  '二级域名开关
	application("dnswww")=rs("SMT_dnswww")                                  'www	
	Application("citydns")=rs("SMT_citydns")                                '二级域名后缀
	Application.UnLock
'	Application("citybottom")=rs("SMT_citybottom")
'	Application("citycss")=rs("SMT_citycss")
'	Application("citygg")=rs("Ald_citygg")				                   '网站公告管理
'	Application("cityurl")=rs("SMT_cityurl")
'	Application("cityname")=rs("SMT_cityname")
'	Application("citytitle")=rs("SMT_citytitle")

'	application("cityxxkey")=rs("SMT_cityxxkey")                            '行业新闻是否确认
'	application("cityjobzpkey")=rs("SMT_cityjobzpkey")                      '企业招聘是否开通
'	application("cityjobqzkey")=rs("SMT_cityjobqzkey")                      '人才添加是否开通
'	application("cityfreejob")=rs("SMT_cityfreejob")                        '免费人才库有效时间
'	application("citywlzpkey")=rs("SMT_citywlzpkey")						'货源是否开通
'	application("citywlqzkey")=rs("SMT_citywlqzkey")                        '空车添加是否开通
'	application("cityfreewl")=rs("SMT_cityfreewl")                          '免费空车库有效时间	

'	application("citymailsmtp")=rs("SMT_citymailsmtp") 
'	application("citymaillog")=rs("SMT_citymaillog") 
'	application("citymailpass")=rs("SMT_citymailpass") 
'	application("citymaildns")=rs("SMT_citymaildns") 
'	application("citymailkey")=rs("SMT_citymailkey") '	
'	application("citysy")=rs("SMT_citysy")                                  '开通试用
'	application("citysydate")=rs("SMT_citysydate")                          '试用用户注册试用多少天
'	application("cityusertk")=rs("SMT_cityusertk")
'	application("cityviptk")=rs("SMT_cityviptk")
'	application("cityeurl")=rs("SMT_cityeurl")
'	application("citymail")=rs("SMT_citymail")
'	application("citysearch")=rs("SMT_citysearch")
'	application("Powered")=rs("SMT_Powered")
'	Application("cityabout")=rs("SMT_cityabout")
'	Application("citycontact")=rs("SMT_citycontact")
'	Application("citybuy")=rs("SMT_citybuy")
'	application("cityadmin")=rs("SMT_cityadmin")
'	application("cityHTML")=rs("SMT_cityHTML")
'   application("urlKeyWord")=rs("SMT_keyword")
'	application("urlDesCription")=rs("SMT_description")

end if
rs.close
set rs=nothing
end if
	citysypkey=Application("citysypkey")
    cityypkey=Application("cityypkey") '企业注册是否要后台审核
    cityvipypkey=Application("cityvipypkey")
    cityscakey=Application("cityscakey")  '发布商机是否要后台审核
    cityscafree=Application("cityscafree")'发布免费商机是否要后台审核
	citynewskey=Application("citynewskey")'动态新闻是否需要审核	
	cityscayx=Application("cityscayx")'免费商机是否要审核
	cityxxkey=application("cityxxkey")'行业新闻是否需要审核
	cityping=Application("cityping")'要屏蔽的字符
	citypingname=Application("citypingname")'禁止注册的用户名
	citycpkey=application("citycpkey")'产品是否需要要后台开通
	flag=application("flag")
	webkey=application("webkey")'网站开关
	citysyvipdate=application("citysyvipdate")'VIP会员可以试用多少天
	cityuserp=application("cityuserp")'普通会员是否要收费
	cityuserpm=application("cityuserpm")'普通会员多少钱一年
	cityvipp=application("cityvipp")'VIP会员是否要收费
	cityvippm=application("cityvippm")'VIP会员要多少钱一年
    cityjysj=application("cityjysj")'在几个工作日内交齐费用
	citysql=application("citysql")
	OKAr=split(Application("OKAr"),",")
	OKsize=int(Application("OKSize"))
	OKArpic=split(Application("OKArpic"),",")
	OKsizepic=int(Application("OKsizepic"))	
	dnskey=application("dnskey")
	dnswww=application("dnswww")
	citydns=Application("citydns")'二级域名后缀	
	
'   citycss=Application("citycss")                                    '叠层样式表
'	citygg=Application("citygg")
'	citygg=replace(citygg,"2005年08月20日","")
'    cityurl=Application("cityurl")                                     '网站网址
'	cityeurl=application("cityeurl")'网站缩写
'    cityname=Application("cityname")                                   '网站名称
'    citytitle=Application("citytitle")                                 '网站标题
'    

'	cityjobzpkey=application("cityjobzpkey")'企业招聘是否开通
'	cityjobqzkey=application("cityjobqzkey")'人才添加是否开通
'	cityfreejob=application("cityfreejob")'免费人才库有效时间
'	citywlzpkey=application("citywlzpkey")'货源是否开通
'	citywlqzkey=application("citywlqzkey")'空间添加是否开通
'	cityfreewl=application("cityfreewl")'免费空车库有效时间
'	

'	Powered=application("Powered")
	
	
	'邮件功能
'	citymailsmtp=application("citymailsmtp")
'	citymaillog=application("citymaillog")
'	citymailpass=application("citymailpass")
'	citymaildns=application("citymaildns")
'	citymailkey=application("citymailkey")
'	
'	citysy=application("citysy")'开通试用
'	citysydate=application("citysydate")'试用会员可以注册多少天
	
	
	
'	citymail=application("citymail")
	'cityusertk=code_bl(application("cityusertk"))
'	cityviptk=code_bl(application("cityviptk"))
'    citybottom=code_bl(Application("citybottom"))
'	citysearch=code_bl(application("citysearch"))
	
	'cityabout=code_bl(Application("cityabout"))
'	citycontact=code_bl(Application("citycontact"))
'	citybuy=code_bl(Application("citybuy"))
'	cityadmin=Application("cityadmin")
'	cityHTML=application("cityHTML")
'	urlKeyWord=application("urlKeyWord")
'	urlDescRiption=application("urlDescRiption")
	
'====================================================
'citymailkey=split(citymailkey,",")
'citymailkey0=citymailkey(0)
'citymailkey1=citymailkey(1)
'citymailkey2=citymailkey(2)  
'citymailkey3=citymailkey(3)
'citymailkey4=citymailkey(4)

webkey=split(webkey,"|")
webkey0=webkey(0)                     '是否关闭普通会员注册           
webkey1=webkey(1)                     '关闭普通会员注册的原因
webkey2=webkey(2)                     '是否关闭试用会员注册
webkey3=webkey(3)                     '关闭试用会员注册的原因 
webkey4=webkey(4)                     '是否关闭网站
webkey5=webkey(5)                     '关闭网站的原因
webkey6=webkey(6)                     '是否关闭免费商机功能
webkey7=webkey(7)                     '关闭免费商机功能的原因
webkey8=webkey(8)                     '是否显示友情链接

flag=split(flag,"|")
flag9=flag(9)
flag22=flag(22)
flag26=flag(26)
flag27=flag(27)
flag32=flag(32)
flag0=flag(0)
flag1=flag(1)
flag2=flag(2)
flag3=flag(3)
flag4=flag(4)
flag5=flag(5)
flag6=flag(6)
flag7=flag(7)
flag8=flag(8)
flag10=flag(10)
flag11=flag(11)
flag12=flag(12)
flag13=flag(13)
flag14=flag(14)
flag15=flag(15)
flag16=flag(16)
flag17=flag(17)
flag18=flag(18)
flag19=flag(19)
flag20=flag(20)
flag21=flag(21)
flag23=flag(23)
flag24=flag(24)
flag25=flag(25)

flag28=flag(28)
flag29=flag(29)
flag30=flag(30)
flag31=flag(31)

flag33=flag(33)
flag34=flag(34)
flag35=flag(35)
flag36=flag(36)
flag37=flag(37)
flag38=flag(38)
flag39=flag(39)
flag40=flag(40)
flag41=flag(41)
flag42=flag(42)
flag43=flag(43)
flag44=flag(44)
flag45=flag(45)
flag46=flag(46)
'flag47=flag(47)
'flag48=flag(48)
%>
<%
if webkey4=1 then
response.write(webkey5)
response.End()
end if
weburl="http://www.xsp2.com/"
memberurl="/"

function chkApp()
if chkNull(Application("citysypkey"),1) or chkNull(Application("cityypkey"),1) or chkNull(Application("cityvipypkey"),1) or chkNull(Application("cityscakey"),1) or chkNull(Application("cityscafree"),1) or chkNull(Application("citynewskey"),1) or chkNull(Application("cityscayx"),1) or chkNull(Application("cityping"),1) or chkNull(Application("citysql"),1) or chkNull(Application("citypingname"),1) or chkNull(Application("citysyvipdate"),1) or chkNull(Application("cityuserp"),1) or chkNull(Application("cityuserpm"),1) or chkNull(Application("cityvipp"),1) or chkNull(Application("cityvippm"),1) or chkNull(Application("cityjysj"),1) or chkNull(Application("citycpkey"),1) or chkNull(Application("flag"),1) or chkNull(Application("webkey"),1) or chkNull(Application("OKAr"),1) or chkNull(Application("OKSize"),1) or chkNull(Application("OKArpic"),1) or chkNull(Application("OKSizepic"),1) or chkNull(Application("dnskey"),1) or chkNull(Application("dnswww"),1) or chkNull(Application("citydns"),1)  then
chkApp=false
else
chkApp=true
end if  
end function
%>