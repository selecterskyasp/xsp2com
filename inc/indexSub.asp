﻿<%
'获取公司信息 0最新加盟 2贸信通会员 3大分类下推荐的公司或旺铺
function getCompany(ssum,sfields,swhere,ssort,smode)
dim grs,gsql,tcontent,tcontent1,tcontent2,ti
dim turl,tpic,tt
ti=1
tcontent=""
tcontent1=""
tcontent2=""
if sfields<>"" then sfields=","&sfields
gsql="select top "&ssum&" SMT_id,SMT_coname,smt_user"&sfields&" from SMT_yp where SMT_key<>0 and SMT_key<>2 and SMT_key1=1 "&swhere&" order by "&ssort
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=ssum	
		turl="http://"&grs("SMT_user")&".xsp2.com"
		tt=grs("SMT_coname")		
		select case smode
		case 0
			tcontent=tcontent&"<li>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,15)&"</a> ["&formatdatetime(grs("SMT_date"),2)&"]</li>" & vbcrlf 
		case 1		
			tpic=grs("SMT_logo")	
			tcontent=tcontent&"<li>" & vbcrlf 
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" alt="""&tt&""" /></a>" & vbcrlf 
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,14)&"</a></li>" & vbcrlf 
		case 2
			if ti mod 3=0 then
				tcontent=tcontent&"<p>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,6)&"</a></p>" & vbcrlf 
			elseif ti mod 3=1 then
				tcontent1=tcontent1&"<p>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,6)&"</a></p>" & vbcrlf 
			else
				tcontent2=tcontent2&"<p>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,6)&"</a></p>" & vbcrlf 
			end if
		case 3			
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,6)&"</a> &nbsp;"
		end select
	grs.movenext
	ti=ti+1
	loop	
else
	tcontent="<a href=""javascript:;"">暂无相关信息</a>"
end if
grs.close
set grs=nothing
if smode=2 then
getCompany=tcontent&"||"&tcontent1&"||"&tcontent2
else
getCompany=tcontent
end if
tcontent="":tcontent1="":tcontent2=""
end function



'获取BuySell信息
'sm 1为求购信息 0为最新供应信息 2为推荐供应 3分类产品信息 4分类产品信息图文
function getBuySell(ssum,sfields,swhere,ssort,smode)
dim grs,gsql,tcontent,ti
dim turl,tmode,tt
ti=1
tcontent=""
if smode=1 then 
gsql="select top "&ssum&" SMT_id,SMT_scatitle,SMT_yp_id,SMT_begindate,SMT_picture,SMT_price"&sfields&" from SMT_sca where SMT_key=1 and SMT_key1=1 and SMT_key2=1 "&swhere&" order by "&ssort
tmode="/buy/detail/"
else
gsql="select top "&ssum&" SMT_id,SMT_cpname,SMT_yp_id,SMT_date,SMT_pic,SMT_cpjg"&sfields&" from SMT_cp where SMT_key=1 and SMT_key1=1 and SMT_key2=1 "&swhere&" order by "&ssort
tmode="/sell/detail/"
end if
if sfields<>"" then sfields=","&sfields
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=ssum	
		turl=tmode&grs("SMT_yp_id")&"_"&grs("SMT_id")&".html"
		tt=grs(1)
		select case smode
		case 0
			tcontent=tcontent&"<li><div class=""inforcontent""><a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,10)&"</a></div>" & vbcrlf 
			tcontent=tcontent&"<div class=""infortime"">"&hour(grs(3))&":"&minute(grs(3))&"</div>" & vbcrlf 
			tcontent=tcontent&"</li>" & vbcrlf 			
		case 1
			tcontent=tcontent&"<li>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,10)&"</a>["&formatdatetime(grs(3),2)&"]</li>" & vbcrlf
		case 2
			tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&grs(4)&""" class=""img"" width=""60"" height=""60"" /></a>" & vbcrlf 
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,5)&"</a>" & vbcrlf 
			tcontent=tcontent&"</li>" & vbcrlf 
		case 3
			tcontent=tcontent&"<p>·<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,10)&"</a></p>" & vbcrlf
		case 4
			tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&tt&"""><img src="""&grs(4)&""" class=""img"" width=""60"" height=""60"" /></a>" & vbcrlf 
			tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&left(tt,5)&"</a>" & vbcrlf 
			tcontent=tcontent&"<span class=""red"">批发价："&getprice(grs(5),2)&"元</span>"&vbcrlf
			tcontent=tcontent&"</li>" & vbcrlf 	

		end select
	grs.movenext
	ti=ti+1
	loop
else
	tcontent="暂无相关信息"
end if
grs.close
set grs=nothing
getBuySell=tcontent
tcontent=""
end function

'得到市场信息
function getMarket()
dim grs,gsql,tcontent,tcontent1,ti,tpic
tcontent="":tcontent1=""
snum=16
gsql="select top 16 SMT_title,smt_id from SMT_market where SMT_id>1 order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	ti=1
	do while not grs.eof and ti<=snum
	if ti<=6 then		
	tcontent=tcontent&"<p>·<a href=""/market/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),8)&"</a></p>"&vbcrlf	
	else
	tcontent1=tcontent1&"<p>·<a href=""/market/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),10)&"</a></p>"&vbcrlf	
	end if
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getMarket=array(tcontent,tcontent1):tcontent="":tcontent1=""
end function


'获取知道
function getZhidao()
dim grs,gsql,tcontent,ti
ti=1
gsql="select  top 2 id,title FROM tb_message where isshow=1 ORDER BY id desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=2	
		if ti=1 then	
		tcontent=tcontent&"<li>·<a href=""zhidao/question.asp?id="&grs("id")&""" title="""&grs("title")&""" target=""_blank"" class=""red"">"&left(grs("title"),14)&"</a></li>"&vbcrlf		
		else
		tcontent=tcontent&"<li>·<a href=""zhidao/question.asp?id="&grs("id")&""" title="""&grs("title")&""" target=""_blank"">"&left(grs("title"),14)&"</a></li>"&vbcrlf
		end if
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getZhidao=tcontent:tcontent=""
end function

'推荐新闻
function getNews1(cid)
dim grs,gsql,tcontent,tcontent1,ti,snum
dim turl,tt,tid
ti=1
snum=16
tcontent="":tcontent1=""
set grs=server.CreateObject("adodb.recordset")
grs.open "select top "&snum&" SMT_id,SMT_title,SMT_htmlurl,SMT_date from SMT_xxnews where SMT_key1=1 and SMT_key2=1 and SMT_newssort="&cid&"  order by SMT_id desc",conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum	
		tid=grs("SMT_id")
		turl=grs("SMT_htmlurl")&tid&".html"
		tt=trim(grs("SMT_title"))
		if ti<=6 then
			tcontent=tcontent&"<p>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,8)&"</a></p>"&vbcrlf
		else
			tcontent1=tcontent1&"<p>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,10)&"</a></p>"&vbcrlf	
		end if	
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getNews1=array(tcontent,tcontent1)
end function



'推荐新闻
function getTjNews(snum,cid)
dim grs,gsql,tcontent,ti
dim turl,tt,tid
ti=1
set grs=server.CreateObject("adodb.recordset")
grs.open "select top "&snum&" SMT_id,SMT_title,SMT_htmlurl,SMT_date from SMT_xxnews where SMT_key1=1 and SMT_key2=1 and SMT_newssort="&cid&"  order by SMT_id desc",conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=snum	
		tid=grs("SMT_id")
		turl=grs("SMT_htmlurl")&tid&".html"
		tt=trim(grs("SMT_title"))
		if ti=1 then
			tcontent=tcontent&"<li>·<a href="""&turl&""" class=""red"" title="""&tt&""" target=""_blank"">"&left(tt,13)&"</a></li>"&vbcrlf
		else
			tcontent=tcontent&"<li>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,13)&"</a></li>"&vbcrlf	
		end if	
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getTjNews=tcontent
end function

'推荐展会
function getTjFair()
dim grs,gsql,tcontent,tcontent1,ti
dim turl,tt,tid
ti=1
tcontent="":tcontent1=""
set grs=server.CreateObject("adodb.recordset")
grs.open "select top 16 SMT_id,SMT_z_name,smt_htmlurl,SMT_z_date from SMT_exhibit where SMT_key>0 and SMT_key2=1 order by SMT_z_date desc",conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=16	
		tid=grs("SMT_id")
		turl=grs("SMT_htmlurl")&tid&".html"
		tt=trim(grs("SMT_z_name"))
		if ti<=6 then
			tcontent=tcontent&"<p>·<a href="""&turl&"""  title="""&tt&""" target=""_blank"">"&left(tt,8)&"</a></p>"&vbcrlf
		else
			tcontent1=tcontent1&"<p>·<a href="""&turl&""" title="""&tt&""" target=""_blank"">"&left(tt,10)&"</a></p>"&vbcrlf
		end if	
		grs.movenext
		ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getTjFair=array(tcontent,tcontent1):tcontent="":tcontent1=""
end function

'图片友情链接
function getPicLink()
dim grs,gsql,tcontent,ti
ti=1
tcontent="<div id=""List1_1"">"
gsql="select top 18 SMT_linkname,SMT_linkurl,smt_linksm,smt_id from SMT_links where SMT_key=1 and SMT_key1=1 order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=18	
	tcontent=tcontent&"<a href="""&grs("SMT_linkurl")&""" class=""pl"" target=""_blank""><img src="""&grs("SMT_linkname")&""" width=""104"" height=""46"" border=""0"" ></a>"&vbcrlf
	if ti=9 then tcontent=tcontent&"</div><div id=""List2_1"">"&vbcrlf
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
tcontent=tcontent&"</div>"&vbcrlf
if ti<=9 then tcontent=tcontent&"<div id=""List2_1""></div>"&vbcrlf
grs.close
set grs=nothing
getPicLink=tcontent
end function

'图片友情链接
function getTxtLink()
dim grs,gsql,tcontent,ti
ti=1
tcontent=""
gsql="select top 30 SMT_linkname,SMT_linkurl,smt_id from SMT_links where SMT_key=0 and SMT_key1=1 order by SMT_id desc"
set grs=server.CreateObject("adodb.recordset")
grs.open gsql,conn,1,1
if not(grs.bof and grs.eof) then
	do while not grs.eof and ti<=30
	tcontent=tcontent&"<a href="""&grs("SMT_linkurl")&""" target=""_blank"">"&grs("SMT_linkname")&"</a>　"&vbcrlf	
	grs.movenext
	ti=ti+1
	loop
else
	tcontent=tcontent&"暂无相关信息"
end if
grs.close
set grs=nothing
getTxtLink=tcontent
end function

'获取论坛的信息
function getBBsBroard(snum,sbid)
dim trs,i,tcontent
dim tt,tt1,turl,maxlen,tlen
maxlen=13
set trs=server.CreateObject("adodb.recordset")
if sbid=0 then
sql="select top "&snum&" rootid,topic,dateandtime,boardid from dv_bbs1 where parentID=0 order by dateandtime desc"
else
sql="select top "&snum&" rootid,topic,dateandtime,boardid from dv_bbs1 where parentID=0 and boardID="&sbid&" order by announceID desc"
end if
trs.open sql,bbsConn,1,1
if trs.bof and trs.eof then
	getBBsBroard="<a href=""javascript:;"">暂无相关信息</a>"
	trs.close
	set tr=nothing
	exit function
end if
i=0
tlen=0
do while not trs.eof and i<snum
turl="http://bbs.xsp2.com/dispbbs.asp?boardid="&trs("boardid")&"&Id="&trs("rootid")
tt=trs("topic")	
tlen=tlen+len(tt)
if tlen>maxlen then
	if len(tt)-(tlen-maxlen)>0 then
	tt1=left(tt,len(tt)-(tlen-maxlen))
	else
	getBBsBroard=tcontent:tcontent=""
	exit function
	end if
else
	tt1=tt
end if
tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&tt&""">"&tt1&"</a> &nbsp;"
i=i+1
trs.movenext
loop
trs.close
set trs=nothing
getBBsBroard=tcontent:tcontent=""
end function

function getIndex()
dim gStr,template
dim tkeyword,tdescription,ttitle
dim tmparr
tkeyword="网店代理 网店货源 网店加盟 免费代理 义乌小商品批发 义乌小商品市场 永康五金批发 义乌市场导航 义乌货源导航 中国批发市场导航 网店货源导航 中国网店代销 中国货源导航 永康五金市场 义乌小商品城 义乌库存批发 义乌物流网"
tdescription="做专业的中国义乌小商品批发，永康五金批发 网店代理，网店加盟，免费代理，免费代销，义乌库存，批发市场导航，义乌货源导航，义乌货源，全国批发市场导航，中国批发市场导航，中国库存信息，物流市场，市场新闻等综合信息交流推广服务平台。"
ttitle="大中华市场网 义乌小商品代理 永康五金城 义乌小商品市场 网店代理 网店加盟 网店货源 批发市场"

template=ReadFromUTF("/template/home/index.html","utf-8")
template=replace(template,"$head",head(tkeyword,tdescription,ttitle,"",0)&vbcrlf)
template=replace(template,"$热点资讯$",getTjNews(2,36))
template=replace(template,"$行业资讯$",getTjNews(2,15))
template=replace(template,"$市场新闻$",getTjNews(2,11))
template=replace(template,"$展会资讯$",getTjNews(2,17))
template=replace(template,"$问知堂$",getzhidao())
template=replace(template,"$供应信息$",getBuySell(10,"","","SMT_id desc",0))
template=replace(template,"$信誉旺铺13$",getCompany(2,"","  and SMT_ypxxone_id=13 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺4$",getCompany(2,"","  and SMT_ypxxone_id=4 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺6$",getCompany(2,""," and SMT_ypxxone_id=6 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺28$",getCompany(2,"","  and SMT_ypxxone_id=28 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺29$",getCompany(2,"","  and SMT_ypxxone_id=29 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺16$",getCompany(2,""," and SMT_ypxxone_id=16 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺5$",getCompany(2,"","  and SMT_ypxxone_id=5 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺7$",getCompany(2,""," and SMT_ypxxone_id=7 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺14$",getCompany(2,"","  and SMT_ypxxone_id=14 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺17$",getCompany(2,"","  and SMT_ypxxone_id=17 and SMT_key=3 and SMT_vip=2","SMT_id desc",3))
template=replace(template,"$信誉旺铺13$",getCompany(2,""," and SMT_cojy='市场经销商'","SMT_id desc",3))

gstr=getCompany(18,""," and SMT_cojy='供应商' and SMT_key=3 and SMT_vip=2","SMT_id desc",2)
tmparr=split(gstr,"||")
template=replace(template,"$贸信通会员1$",tmparr(0))
template=replace(template,"$贸信通会员2$",tmparr(1))
template=replace(template,"$贸信通会员3$",tmparr(2))
erase tmparr
'template=replace(template,"$推荐小商品$",getBuySell(10,""," and SMT_key3=1 and SMT_pic<>'' and not SMT_pic  is null","SMT_id desc",2))
template=replace(template,"$推荐小商品$",GetHttpPage("http://www.xsp2.cn/common/inc/xsp2comgetTjProducts.asp"))
gstr=GetHttpPage("http://www.58tj.net/inc/getxsphomeproducts.asp")
tmparr=split(gstr,"||")
template=replace(template,"$库存信息1$",tmparr(0))
template=replace(template,"$库存信息2$",tmparr(1))
template=replace(template,"$库存信息3$",tmparr(2))
erase tmparr
template=replace(template,"$诚信网店$",getUrlList(56," and charindex('3,',sortpath)=1","adddate desc",0))
tmparr=getMarket()
template=replace(template,"$批发市场1$",tmparr(0))
template=replace(template,"$批发市场2$",tmparr(1))
erase tmparr
tmparr=getTjfair()
template=replace(template,"$展会信息1$",tmparr(0))
template=replace(template,"$展会信息2$",tmparr(1))
erase tmparr
template=replace(template,"$社区精华50$",getBBsBroard(2,50))
template=replace(template,"$社区精华56$",getBBsBroard(2,56))
template=replace(template,"$社区精华57$",getBBsBroard(2,57))
template=replace(template,"$社区精华14$",getBBsBroard(2,14))
template=replace(template,"$社区精华35$",getBBsBroard(2,35))
template=replace(template,"$社区精华36$",getBBsBroard(2,36))
template=replace(template,"$社区精华29$",getBBsBroard(2,29))
template=replace(template,"$社区精华25$",getBBsBroard(2,25))
template=replace(template,"$社区精华39$",getBBsBroard(2,39))
template=replace(template,"$社区精华41$",getBBsBroard(2,41))
template=replace(template,"$义乌市场分类$",getClassCompany(4,0))
template=replace(template,"$永康五金分类$",getClassCompany(5,0))
tmparr=getnews1(11)
template=replace(template,"$市场营销1$",tmparr(0))
template=replace(template,"$市场营销2$",tmparr(1))
erase tmparr
template=replace(template,"$piclink",getPicLink())
template=replace(template,"$txtlink",getTxtLink())
template=replace(template,"$updatetime",now())
getIndex=template
template=""
tkeyword="":tdescription="":ttitle=""
'template=replace(template,"$最新加盟",getcompany(10,"SMT_date"," and SMT_cojy='供应商' and SMT_vip=2","SMT_id desc",0))
'template=replace(template,"$求购信息",getBuySell(10,"","","SMT_id desc",1,1))

'template=replace(template,"$小商品知道",getZhidao())
'template=replace(template,"$展会信息",getTjFair())
'template=replace(template,"$经济资讯",getNews(8,"SMT_newssort=13",1))
'template=replace(template,"$商务指南",getNews(8,"SMT_newssort=12",1))
'template=replace(template,"$热点资讯",getNews(2,"SMT_newssort=36 and SMT_id<>9320",0))
'template=replace(template,"$最新供应信息",gethotSellBuy(10))
'template=replace(template,"$行业分类",getClass())
'template=replace(template,"$供应信息",getBuySell(6,"","","SMT_id desc",0,0))
'template=replace(template,"$贸信通会员",getcompany(12,""," and SMT_vip=2 and SMT_cojy='供应商'"," SMT_tjdate desc",2))
'template=replace(template,"$推荐产品",getTjProducts())
'template=replace(template,"$市场旺铺",getWangpu())
'template=replace(template,"$批发商城",getHttpPage("http://www.xsp2.cn/common/inc/xsp2comgetTjProducts.asp"))
'template=replace(template,"$库存中心",getHttpPage("http://www.58tj.net/inc/getxsptjProducts.asp"))
'template=replace(template,"$货运中心",getWl(4))
end function

'mpath: 生成的路径 为空时为/
function createIndexhtml(mpath)
dim template
template=getindex()
if mpath="" then mpath="/"
call SaveToFile(template,mpath&"index.html")
call SaveToFile(template,mpath&"google.htm")
call SaveToFile(template,mpath&"baidu.htm")
template=""
end function

''首页获取物流货运
'function getWl(byVal ssum)
'dim grs,tcontent,ti
'set grs=server.CreateObject("adodb.recordset")
'grs.open "select top "&ssum&" SMT_id,SMT_companyname,SMT_from,SMT_to,SMT_cid,smt_flx from wl_line where SMT_key=1 and SMT_flag=1 order by SMT_date,SMT_id desc",conn,1,1
'if not(grs.bof and grs.eof) then
'ti=1
'tcontent=""
'do while not grs.eof and ti<=ssum
'tcontent=tcontent&""&vbcrlf
'tcontent=tcontent&"<li>"&vbcrlf
'tcontent=tcontent&"<h2>"&vbcrlf
'tcontent=tcontent&"<a title=""从"&grs("SMT_from")&"到"&grs("SMT_to")&"的货运,由"&grs("SMT_companyname")&"公司发布"" href=""http://www.56zhw.com/line/detail.asp?id="&grs("SMT_id")&""" target=""_blank"">"&left(grs("SMT_from"),2)&"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&left(grs("SMT_to"),2)&"</a>"&vbcrlf
'tcontent=tcontent&"</h2>"&vbcrlf
'tcontent=tcontent&left(grs("SMT_companyname"),8)&vbcrlf
'tcontent=tcontent&"</li>"&vbcrlf
'grs.movenext
'ti=ti+1
'loop
'else
'tcontent="暂无相关信息"
'end if
'grs.close
'set grs=nothing
'getWl=tcontent:tcontent=""
'end function
'
'function getClass()
'dim grs,grs1,gsql,tcontent,ti,tj
'dim tlen
'dim tstr
'dim tclassname
'ti=1
'tlen=0
'tcontent="<div class=""category"">"&vbcrlf
'tcontent=tcontent&"<ul>"&vbcrlf
'gsql="select SMT_ypxxone_id,SMT_ypxxone from SMT_ypxxone order by SMT_date desc"
'set grs=server.CreateObject("adodb.recordset")
'set grs1=server.CreateObject("adodb.recordset")
'grs.open gsql,conn,1,1
'if not(grs.bof and grs.eof) then
'	tj=1
'	do while not grs.eof and tj<=12
'		if tj mod 2 =0 then tstr="2" else tstr=""
'		tcontent=tcontent&"<li class=""left"&tstr&""">"&vbcrlf
'		tcontent=tcontent&"<h2><a href=""/sell/?bid="&grs("SMT_ypxxone_id")&""" target=""_blank"">"&grs("SMT_ypxxone")&"</a>"&vbcrlf
'		grs1.open "select SMT_ypxxtwo,SMT_ypxxtwo_id from SMT_ypxxtwo where SMT_ypxxone_id="&grs("SMT_ypxxone_id"),conn,1,1
'		if not grs1.eof then
'			ti=1
'			tlen=0
'			do while not grs1.eof					
'				if tlen>23 then exit do	
'				if ti<3 then
'				tcontent=tcontent&"&nbsp;<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a>"&vbcrlf
'				elseif ti=3 then
'				tcontent=tcontent&"</h2>"&vbcrlf
'				tcontent=tcontent&"<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a> <span class=""blue"">|</span> "&vbcrlf
'				else
'				tcontent=tcontent&"<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a> <span class=""blue"">|</span> "&vbcrlf
'				tlen=tlen+len(trim(grs1("SMT_ypxxtwo")))
'				end if					
'				ti=ti+1		
'				grs1.movenext							
'			loop		
'			if ti<3 then tcontent=tcontent&"</h2>"&vbcrlf				
'		else
'			tcontent=tcontent&"</h2>"&vbcrlf	
'		end if
'		grs1.close
'		tcontent=tcontent&" <a href=""/sell/?bid="&grs("SMT_ypxxone_id")&""" target=""_blank"">更多</a> "&vbcrlf	
'		tcontent=tcontent&"</li>"&vbcrlf			
'		grs.movenext		
'		if not grs.eof then
'			tcontent=tcontent&"<li class=""right"&tstr&""">"&vbcrlf
'			tcontent=tcontent&"<h2><a href=""/sell/?bid="&grs("SMT_ypxxone_id")&""" target=""_blank"">"&grs("SMT_ypxxone")&"</a>"&vbcrlf
'			grs1.open "select SMT_ypxxtwo,SMT_ypxxtwo_id from SMT_ypxxtwo where SMT_ypxxone_id="&grs("SMT_ypxxone_id"),conn,1,1
'			if not grs1.eof then
'				ti=1
'				tlen=0
'				do while not grs1.eof					
'					if tlen>23 then exit do				
'					if ti<3 then
'					tcontent=tcontent&"&nbsp;<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a>"&vbcrlf
'					elseif ti=3 then
'					tcontent=tcontent&"</h2>"&vbcrlf
'					tcontent=tcontent&"<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a> <span class=""blue"">|</span> "&vbcrlf
'					else
'					tcontent=tcontent&"<a href=""/sell/?bid="&grs("SMT_ypxxone_id")&"&sid="&grs1("SMT_ypxxtwo_id")&""" target=""_blank"">"&grs1("SMT_ypxxtwo")&"</a> <span class=""blue"">|</span> "&vbcrlf
'					tlen=tlen+len(trim(grs1("SMT_ypxxtwo")))	
'					end if						
'					ti=ti+1		
'					grs1.movenext	
'				loop		
'				if ti<3 then tcontent=tcontent&"</h2>"&vbcrlf					
'			else
'				tcontent=tcontent&"</h2>"&vbcrlf	
'			end if
'			grs1.close
'			tcontent=tcontent&" <a href=""/sell/?bid="&grs("SMT_ypxxone_id")&""" target=""_blank"">更多</a> "&vbcrlf	
'			tcontent=tcontent&"</li>"&vbcrlf
'		end if
'		if tj=6 then
'		tcontent=tcontent&"</ul>"&vbcrlf
'		tcontent=tcontent&"</div>"&vbcrlf
'		tcontent=tcontent&"<div class=""categoryadver""><a href=""http://www.xsp2.cn/products/?keyword=%e9%ad%94%e6%9c%af&id=%e6%89%80%e6%9c%89%e5%88%86%e7%b1%bb&page=2"" target=""_blank""><img src=""images/categoryadver.gif"" width=""630"" height=""84"" border=""0"" /></a></div>"&vbcrlf
'		tcontent=tcontent&"<div class=""category"">"&vbcrlf
'		tcontent=tcontent&"<ul>"&vbcrlf
'		end if
'		grs.movenext
'		tj=tj+1
'		loop
'else
'	tcontent=tcontent&"暂无相关信息"
'end if
'tcontent=tcontent&"</ul>"&vbcrlf
'tcontent=tcontent&"</div>"&vbcrlf
'grs.close
'set grs=nothing
'set grs1=nothing
'getClass=tcontent
'end function

''获取商城产品
'function getShangcheng(byVal ssum)
'dim grs,gsql
'dim tcontent,ti,tpic,turl,ttitle
'ti=1
'tcontent=""
'gsql="select top "&ssum&" id,smallpic,title from pf_products where commend=1 order by c_times desc"
'set grs=server.CreateObject("adodb.recordset")
'grs.open gsql,conn,1,1
'if not(grs.bof and grs.eof) then
'	do while not grs.eof and ti<=ssum
'	tpic=grs("smallpic")
'	turl="http://pf.xsp2.com/detail.asp?id="&grs("id")
'	ttitle=left(grs("title"),7)
'	if chkNull(tpic,20) then tpic="images/noimg.gif" else tpic="http://pf.xsp2.com"&tpic
'	tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&grs("title")&"""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" alt="""&grs("title")&""" /></a>"&vbcrlf
'	tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&grs("title")&""">"&ttitle&"</a></li>"&vbcrlf
'	grs.movenext
'	ti=ti+1
'	loop
'else
'	tcontent="暂无相关信息"
'end if
'grs.close
'set grs=nothing
'getShangcheng=tcontent:tcontent=""
'end function
'
''推荐产品
'function getTjProducts()
'dim grs,gsql,tcontent,ti
'dim tpic,ttitle,turl
'ti=1
'tcontent="<div class=""prolist"">"&vbcrlf
'tcontent=tcontent&"<ul>"&vbcrlf
'gsql="select top 11 SMT_cpname,SMT_id,smt_pic,SMT_yp_id from smt_cp where smt_key=1 and (SMT_newpro=0 or SMT_isSetPass=0) and smt_key1=1 and SMT_key3=1 order by SMT_date desc"
'set grs=server.CreateObject("adodb.recordset")
'grs.open gsql,conn,1,1
'if not(grs.bof and grs.eof) then
'	do while not grs.eof and ti<=11
'	tpic=grs("SMT_pic")
'	if chkNull(tpic,20) then tpic="images/noimg.gif"
'	turl="/sell/detail/"&grs("SMT_yp_id")&"_"&grs("SMT_id")&".html"
'	ttitle=grs("SMT_cpname")
'	if ti<3 then
'	tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&ttitle&"""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" /></a>"&vbcrlf
'	tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&ttitle&""">"&left(ttitle,7)&"</a></li>"&vbcrlf
'	elseif ti=3 then
'	tcontent=tcontent&"<li><a href="""&turl&""" target=""_blank"" title="""&ttitle&"""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" /></a>"&vbcrlf
'	tcontent=tcontent&"<a href="""&turl&""" target=""_blank"" title="""&ttitle&""">"&left(ttitle,7)&"</a></li>"&vbcrlf
'	tcontent=tcontent&"</ul>"&vbcrlf
'	tcontent=tcontent&"</div>"&vbcrlf
'	tcontent=tcontent&"<div class=""prolist2"">"&vbcrlf
'	tcontent=tcontent&"<ul>"&vbcrlf
'	else
'	tcontent=tcontent&"<li>·<a href="""&turl&""" target=""_blank"" title="""&ttitle&""">"&left(ttitle,9)&"</a></li>"&vbcrlf
'	end if
'	tcontent=tcontent&""&vbcrlf
'	grs.movenext
'	ti=ti+1
'	loop
'else
'	tcontent=tcontent&"暂无相关信息"
'end if
'tcontent=tcontent&"</ul>"&vbcrlf
'tcontent=tcontent&"</div>"&vbcrlf
'grs.close
'set grs=nothing
'getTjProducts=tcontent:tcontent=""
'end function
'
''市场旺铺
'function getWangpu()
'dim grs,gsql,tcontent,ti
'dim turl,ttitle,tpic,tstr
'tstr="1"
'ti=1
'tcontent=""
'gsql="select top 3 SMT_user,SMT_coname,SMT_coyw,SMT_colxr,SMT_logo from SMT_yp where SMT_key1=1 and smt_vip=2 and SMT_key=3 and SMT_marketid>1 and SMT_marketcid>1 and SMT_cojy='市场经销商' order by SMT_date desc"
'set grs=server.CreateObject("adodb.recordset")
'grs.open gsql,conn,1,1
'if not(grs.bof and grs.eof) then
'	do while not grs.eof and ti<=3
'	turl="http://"&grs("SMT_user")&".xsp2.com"
'	ttitle=grs("SMT_coname")	
'	tpic=grs("SMT_logo")
'	if chkNull(tpic,20) then tpic="images/noimg.gif"
'	if ti mod 2 =0 then tstr="2" else tstr="1"
'	tcontent=tcontent&"<li class=""left0"&tstr&"""><a href="""&turl&""" target=""_blank"" title="""&ttitle&"""><img src="""&tpic&""" width=""80"" height=""80"" border=""0"" class=""img"" /></a></li>"&vbcrlf
'	tcontent=tcontent&"<li class=""right0"&ti&""">"&vbcrlf
'	tcontent=tcontent&"<h3><a href="""&turl&""" target=""_blank"" title="""&ttitle&""">"&left(ttitle,10)&"</a></h3>"&vbcrlf
'	tcontent=tcontent&"摊主："&grs("SMT_colxr")&"<br />"&vbcrlf
'	tcontent=tcontent&"主营："&grs("SMT_coyw")&vbcrlf
'	tcontent=tcontent&"</li>"&vbcrlf
'	grs.movenext
'	ti=ti+1
'	loop
'else
'	tcontent="暂无相关信息"
'end if
'grs.close
'set grs=nothing
'getWangpu=tcontent:tcontent=""
'end function



''==================================================
''函数名：GetHttpPage
''作  用：获取网页源码
''参  数：HttpUrl ------网页地址
''==================================================
'Function GetHttpPage(HttpUrl)
'   If IsNull(HttpUrl)=True Or Len(HttpUrl)<18 Or HttpUrl="$False$" Then
'      GetHttpPage="$False$"
'      Exit Function
'   End If
'   Dim Http
'    Set Http=server.createobject("MSXML2.XMLHTTP")
'   Http.open "GET",HttpUrl,False
'   Http.Send()
'   If Http.Readystate<>4 then
'      Set Http=Nothing 
'      GetHttpPage="$False$"
'      Exit function
'   End if
'   GetHTTPPage=bytesToBSTR(Http.responseBody,"utf-8")
'   Set Http=Nothing
'   If Err.number<>0 then
'      Err.Clear
'   End If
'End Function
'
''==================================================
''函数名：BytesToBstr
''作  用：将获取的源码转换为中文
''参  数：Body ------要转换的变量
''参  数：Cset ------要转换的类型
''==================================================
'Function BytesToBstr(Body,Cset)
'   Dim Objstream
'   Set Objstream = Server.CreateObject("adodb.stream")
'   objstream.Type = 1
'   objstream.Mode =3
'   objstream.Open
'   objstream.Write body
'   objstream.Position = 0
'   objstream.Type = 2
'   objstream.Charset = Cset
'   BytesToBstr = objstream.ReadText 
'   objstream.Close
'   set objstream = nothing
'End Function
''得到市场信息
'function getMarket(byVal smode)
'dim grs,gsql,tcontent,ti,tpic
'dim swhere
'ti=1
'tcontent="<div class=""feature"">"
'swhere=""
'if smode=1 then
'swhere=" and SMT_flag=1"
'end if
'gsql="select top 9 SMT_title,smt_id from SMT_market where SMT_id>1"&swhere&" order by SMT_id desc"
'set grs=server.CreateObject("adodb.recordset")
'grs.open gsql,conn,1,1
'if not(grs.bof and grs.eof) then
'	do while not grs.eof and ti<=9	
'	if ti<4 then	
'	tcontent=tcontent&"·<a href=""http://market.xsp2.com/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),7)&"</a><br />"&vbcrlf
'	elseif ti=4 then
'	tcontent=tcontent&"·<a href=""http://market.xsp2.com/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),7)&"</a>"&vbcrlf	
'	tcontent=tcontent&"</div><div>"&vbcrlf
'	elseif ti>4 and ti<9 then
'	tcontent=tcontent&"·<a href=""http://market.xsp2.com/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),14)&"</a><br />"&vbcrlf
'	else
'	tcontent=tcontent&"·<a href=""http://market.xsp2.com/market.asp?id="&grs("SMT_id")&""" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),14)&"</a>"&vbcrlf
'	end if	
'	grs.movenext
'	ti=ti+1
'	loop
'else
'	tcontent=tcontent&"暂无相关信息"
'end if
'tcontent=tcontent&"</div>"&vbcrlf
'grs.close
'set grs=nothing
'getMarket=tcontent:tcontent=""
'end function
%>
