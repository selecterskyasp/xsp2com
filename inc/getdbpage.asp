<%
'****************************************************
'函数名：getDBValueList
'作  用：按指定sql语句取记录集
'参  数：Sql           传入参数 Sql语句
'返回值:getDBValueList      输出参数 为一个二维数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getdbpage(sql_table,sql_Field,sql_order,sql,pagesize,page,pagecount,recordcount,curcount)

	Dim Comm,Rs
	Dim Recordcounts
	Dim ArrayRecord
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = Conn
	.CommandType = 4
	.CommandText = "page"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@TableName",200,1,254,sql_table)'参数1为定义的变量 2为数据类型 3为数据宽度 4为值
	.Parameters.Append .CreateParameter("@Fields",200,1,300,sql_Field)
	.Parameters.Append .CreateParameter("@OrderField",200,1,300,sql_order)
	.Parameters.Append .CreateParameter("@sqlWhere",200,1,1000,Sql)
	.Parameters.Append .CreateParameter("@pageSize",3,1,8,pagesize)
	.Parameters.Append .CreateParameter("@pageIndex",3,1,8,page)
	.Parameters.Append .CreateParameter("@TotalPage",3,3,8,pagecount)
	.Parameters.Append .CreateParameter("@totalRecord",3,3,8,recordcount)
	Set Rs=.Execute()
	
	End With
	Rs.Close 
	
	pagecount=Comm.Parameters("@TotalPage").value
	recordcount=Comm.Parameters("@totalRecord").value
	curcount=Comm.Parameters("@totalRecord").value
	Rs.Open	
	If curcount=0 Then 
		Redim ArrayRecord(0,0):ArrayRecord(0,0)=0
	Else
		ArrayRecord=Rs.GetRows()
	end if
	Rs.Close
	getdbpage=ArrayRecord
	erase ArrayRecord
	Sql =""
End Function
%>