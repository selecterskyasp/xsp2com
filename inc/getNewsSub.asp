<%
'得到新闻列表 返回查询到的内容 如果没有结果,则返回 暂无相关信息
'snum		条数 
'sfields 	字段(已存在SMT_id,SMT_title,SMT_htmlurl,SMT_date如果有新增则将字段加在后面) 
'swhere		新闻分类
'ssort 		排序方式(默认SMT_date desc)
'smode 		0热点新闻   1推荐新闻分类
function getNews(byVal snum,byVal sswhere,byVal smode)
	dim getinfoRs,grs,getinfoContent,getinfosql
	dim getinfoi	
	dim tmptitle,tmpid,tmpurl,tpic,tmpdate,tmpcontent
	dim tmpsfields,tmpswhere,tmpssort
	tmpsfields="SMT_id,SMT_title,SMT_htmlurl,SMT_date,SMT_picurl"
	tmpswhere="SMT_key1=1 and SMT_key2=1 and "&sswhere
	tmpssort="SMT_date desc"
	getinfosql="select top "&snum&" "&tmpsfields&" from SMT_xxnews where "&tmpswhere&" order by "&tmpssort
	getinfoContent=""
	if smode=1 then
		getinfoContent="<div class=""feature"">"
		set grs=server.CreateObject("adodb.recordset")
		grs.open "select top 1 smt_id,smt_title,SMT_htmlurl,SMT_picURL from smt_xxnews where  SMT_pic=1 and SMT_key=1 and SMT_key1=1 and SMT_key2=1 and SMT_key3=6 and "&sswhere&" and SMT_picurl<>'' order by SMT_id desc",conn,1,1
		if not grs.eof then
		getinfoContent=getinfoContent&"<a href="""&grs("SMT_htmlurl")&grs("SMT_id")&".html"" target=""_blank"" title="""&grs("SMT_title")&"""><img src="""&grs("SMT_picurl")&""" width=""106"" height=""80"" border=""0"" class=""img"" alt="""&grs("SMT_title")&""" /></a>"&vbcrlf
	getinfoContent=getinfoContent&"·<a href="""&grs("SMT_htmlurl")&grs("SMT_id")&".html"" title="""&grs("SMT_title")&""" target=""_blank"">"&left(grs("SMT_title"),13)&"</a><br />"&vbcrlf
		else
		getinfoContent=getinfoContent&"<a href=""javascrpt:;""><img src=""images/noimg.gif"" width=""106"" height=""80"" border=""0"" class=""img"" /></a>"&vbcrlf
		getinfoContent=getinfoContent&"·<a href=""javascript:;"">暂无图片推荐</a><br />"&vbcrlf
		end if
		grs.close
		set grs=nothing	
	end if
	set getinfoRs=server.createobject("adodb.recordset")
'	response.write(getinfoSql)
'	response.end
	getinfoRs.open getinfoSql,conn,1,1
	if getinfoRs.bof and getinfoRs.eof then	
		if 	smode=1 then
		getinfoContent=getinfoContent&"</div><div></div>"	
		else
		getinfoContent="暂无相关信息"
		end if
	else	
	getinfoi=1		
	do while not getinfoRs.eof and getinfoi<=snum		
		tmpid=getinfoRs("SMT_id")
		tmpurl=getinfoRs("SMT_htmlurl")&tmpid&".html"
		tmpdate=getinfoRs("SMT_date")
		select case smode
			case 0:
				tmpTitle=left(getinfoRs("SMT_title"),15)				
				getinfoContent=getinfoContent&"<li>·<a href="""&tmpurl&""" target=""_blank"">"&tmpTitle&"</a>["&formatdatetime(getinfoRs("SMT_date"),2)&"]</li>"&vbcrlf
			case 1:
				tmpTitle=trim(getinfoRs("SMT_title"))
				tmpurl=getinfoRs("SMT_htmlurl")&getinfoRs("SMT_id")&".html"
				if getinfoi<3 then					
				getinfoContent=getinfoContent&"·<a href="""&tmpurl&""" target=""_blank"">"&left(tmpTitle,13)&"</a><br />"&vbcrlf	
				elseif getinfoi=3 then
				getinfoContent=getinfoContent&"·<a href="""&tmpurl&""" target=""_blank"">"&left(tmpTitle,13)&"</a>"&vbcrlf			
				getinfoContent=getinfoContent&"</div><div>"
				elseif getinfoi>3 and getinfoi<snum then
				getinfoContent=getinfoContent&"·<a href="""&tmpurl&""" target=""_blank"">"&left(tmpTitle,16)&"</a> ["&formatdatetime(tmpdate,2)&"]<br />"&vbcrlf	
				else 
				getinfoContent=getinfoContent&"·<a href="""&tmpurl&""" target=""_blank"">"&left(tmpTitle,16)&"</a> ["&formatdatetime(tmpdate,2)&"]"&vbcrlf				
				end if			
		end select
		getinfoRs.movenext
		getinfoi=getinfoi+1
	loop
	end if
	getinfoRs.close
	set getinfoRs=nothing
	if smode=1 then
	getinfoContent=getinfoContent&"</div>"
	end if	
	getNews=getinfoContent
end function
%>
